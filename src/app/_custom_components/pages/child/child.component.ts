import { Component, OnInit } from '@angular/core';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  color = 'defaultdark';
  showSettings = false;
  showMinisidebar = false;
  showDarktheme = false;
  public config: PerfectScrollbarConfigInterface = {};
  constructor() { }
  toggleSidebar() {
    this.showMinisidebar = !this.showMinisidebar;
  }

  ngOnInit() {
  }

}
