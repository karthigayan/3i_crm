import { Component, OnInit } from '@angular/core';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-full',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.css']
})
export class FullComponent implements OnInit {
  color = 'defaultdark';
  showSettings = false;
  showMinisidebar = false;
  showDarktheme = false;
  public config: PerfectScrollbarConfigInterface = {};
  constructor() { }
  toggleSidebar() {
    this.showMinisidebar = !this.showMinisidebar;
  }


  ngOnInit() {
  }

}
