import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [

  {
    path: '',
    title: 'Prospect',
    icon: 'mdi mdi-equal',
    class: 'has-arrow',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: [
      { path: '/prospect', title: 'Overview', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
      { path: '/prospect/myprospect', title: 'My Prospect', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    ]
  },
  {
    path: '',
    title: 'Customer',
    icon: 'mdi mdi-message-bulleted',
    class: 'has-arrow',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: [
      { path: '/customers/customer-overview', title: 'Overview', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
      { path: '/customers', title: 'My Customer', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },

    ]
  },
  {
    path: '',
    title: ' Potentials',
    icon: 'fas fa-bullseye',
    class: 'has-arrow',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: [
      { path: '/potentials', title: 'Overview', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
      { path: '/potentials/mypotentials', title: 'My Potential', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },

    ]
  },
  {
    path: '',
    title: ' Engagement',
    icon: 'fas fa-hands-helping',
    class: 'has-arrow',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: [
      { path: '/engagement', title: 'Overview', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
      { path: '/engagement/myactivities', title: 'My Activities', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },

    ]
  }
];
