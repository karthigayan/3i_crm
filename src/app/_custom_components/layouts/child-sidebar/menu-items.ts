import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    {
    path: '/overview/customer',
    title: 'Investment Overview',
    icon: 'mdi mdi-backburger',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/overview/drilldown',
    title: 'Account Drilldown',
    icon: 'mdi mdi-equal',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/overview/holding',
    title: 'Holding',
    icon: 'fas fa-piggy-bank',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/overview/transactions',
    title: 'Transactions',
    icon: 'mdi mdi-arrange-bring-to-front',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  }
];
