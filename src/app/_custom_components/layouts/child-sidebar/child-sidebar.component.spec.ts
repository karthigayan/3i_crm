import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSidebarComponent } from './child-sidebar.component';

describe('ChildSidebarComponent', () => {
  let component: ChildSidebarComponent;
  let fixture: ComponentFixture<ChildSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
