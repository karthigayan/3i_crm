import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {ROUTES} from '../child-sidebar/menu-items';
import customer from '../../../_modules/childApp/overview/investment-overview/customer.json';
import {CustomerDetailService} from '../../../_service/customer/customer-detail.service';

@Component({
  selector: 'app-child-sidebar',
  templateUrl: './child-sidebar.component.html',
  styleUrls: ['./child-sidebar.component.css']
})
export class ChildSidebarComponent implements OnInit {

  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems: any[];
  public customerId: any;
  public selectedCustomer: any;
  // this is for the open close
  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  constructor(
      private modalService: NgbModal,
      private router: Router,
      private route: ActivatedRoute,
      private custId: CustomerDetailService
  ) {
    this.customerId = custId.getMyCustomer();
    this.selectedCustomer = customer.filter( val => parseInt(val.Customer_ID, 10) === this.customerId);
  }
  // End open close
  ngOnInit() {
    this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
  }
  addActiveClass(element: any) {
    if (element === this.showSubMenu) {
      this.showSubMenu = '0';
    } else {
      this.showSubMenu = element;
    }
  }

}
