import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header-navigation',
  templateUrl: './header-navigation.component.html',
  styleUrls: ['./header-navigation.component.css']
})
export class HeaderNavigationComponent implements AfterViewInit {
  @Output() toggleSidebar = new EventEmitter<void>();

  public config: PerfectScrollbarConfigInterface = {};
  constructor(private modalService: NgbModal) {}

  public showSearch = false;

  // This is for Notifications
  notifications  = [
    {
      round: 'round-danger',
      icon: 'mdi mdi-coin custom_md_icons',
      title: 'Transaction',
      subject: "Joe's Purchase Order for Equity failed, Check why",
      time: '9:30 AM'
    },
    {
      round: 'round-success',
      icon: 'mdi mdi-human custom_md_icons',
      title: 'Life Event',
      subject: 'You added a Family Member for Madelynn',
      time: '9:10 AM'
    },
    {
      round: 'round-info',
      icon: 'mdi mdi-cached  custom_md_icons',
      title: 'Service Update',
      subject: 'John from Operations closed a service ticket successfully',
      time: '9:08 AM'
    },
    {
      round: 'round-primary',
      icon: 'mdi mdi-rotate-right custom_md_icons',
      title: 'Social Media Update',
      subject: 'Maria Doe just tweeted, you should see it',
      time: '9:00 AM'
    }
  ];

  // This is for Mymessages
  mymessages = [
    {
      useravatar: 'assets/images/users/1.jpg',
      status: 'online',
      from: 'Pavan kumar',
      subject: 'Just see the my admin!',
      time: '9:30 AM'
    },
    {
      useravatar: 'assets/images/users/2.jpg',
      status: 'busy',
      from: 'Sonu Nigam',
      subject: 'I have sung a song! See you at',
      time: '9:10 AM'
    },
    {
      useravatar: 'assets/images/users/2.jpg',
      status: 'away',
      from: 'Arijit Sinh',
      subject: 'I am a singer!',
      time: '9:08 AM'
    },
    {
      useravatar: 'assets/images/users/4.jpg',
      status: 'offline',
      from: 'Pavan kumar',
      subject: 'Just see the my admin!',
      time: '9:00 AM'
    }
  ];

  ngAfterViewInit() {}

}
