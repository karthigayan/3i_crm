import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YourPerformanceComponent } from './your-performance/your-performance.component';
import {ChartistModule} from 'ng-chartist';
import {ChartsModule} from 'ng2-charts';
import { ChildOverviewChartComponent } from './child-overview-chart/child-overview-chart.component';
import {RadialGaugeModule} from "@progress/kendo-angular-gauges";

@NgModule({
  declarations: [YourPerformanceComponent, ChildOverviewChartComponent],
    exports: [
        YourPerformanceComponent,
        ChildOverviewChartComponent
    ],
    imports: [
        CommonModule,
        ChartistModule,
        ChartsModule,
        RadialGaugeModule
    ]
})
export class ChartModule { }
