import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist/dist/chartist.component';
import ApexCharts from 'apexcharts';
declare var require: any;
const data: any = require('./data.json');
export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}
@Component({
  selector: 'app-your-performance',
  templateUrl: './your-performance.component.html',
  styleUrls: ['./your-performance.component.css']
})
export class YourPerformanceComponent implements OnInit {
  public distchart: any;
  public incomeDistchart: any;
  public aumochart: any;
  public incomeChart: any;
  public investChart: any;
  public investValueChart: any;
  public customerCountChart: any;
  public convChart: any;
  public distLabel: any;
  public incomeDistLabel: any;
  public aumLabel: any;
  public incomeLabel: any;
  public investLabel: any;
  public valueLabel: any;
  public customerCountLabel: any;
  public convLabel: any;
  public distValue = [];
  public incomeDistValue = [];
  public aumValue = [];
  public incomeValue = [];
  public investValue = [];
  public valueValue = [];
  public customerCountValue = [];
  public convValue = [];
  constructor() {}

  ngOnInit() {
    this.aumTrendChart();
    this.distChange('1');
    this.incomeDistChange('1');
    this.aumChange('1');
    this.incomeChange('1');
    this.investChange();
    this.valueChartChange();
    this.countChange();
    this.convChange();
  }
  // -------------------- Area Chart Function ------------------
  aumTrendChart() {
    const aumoptions = {
      chart: {
        toolbar: {
          show: false
        },
        height: 280,
        type: 'area',
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      series: [{
        name: 'Trend',
        data: this.aumValue
      }],
      labels: this.aumLabel,
      xaxis: {
        categories: this.aumLabel,
      },
      yaxis: {
        opposite: false,
        labels: {
          formatter(value) {
            return (value / 1000000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
        },
      },
      legend: {
        horizontalAlign: 'left'
      }
    };
    this.aumochart = new ApexCharts(
        document.querySelector('#aumTrend'), aumoptions
    );
    this.aumochart.render();
  }
  aumChange(event) {
    if (this.aumochart !== undefined) {
      this.aumochart.destroy();
    }
    this.aumValue = [];
    this.aumLabel = [];
    if (event === '1') {
      data.amu_trends1.forEach(obj => {
        this.aumValue.push(parseInt(obj.AUM, 10));
        this.aumLabel.push(obj.Period);
      });
    } else if (event === '2') {
      data.amu_trends2.forEach(obj => {
        this.aumValue.push(parseInt(obj.AUM, 10));
        this.aumLabel.push(obj.Period);
      });
    }
    this.aumTrendChart();
  }
  // -------------------- Area Chart Function Ends ------------

  // --------------------- Donut Chart Start --------------------
  distributionChart() {
    const distOptions = {
      labels: this.distLabel,
      dataLabels: {
        enabled: false
      },
      chart: {
        type: 'donut',
        height: 319
      },
      series: this.distValue,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 150,
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };
    this.distchart = new ApexCharts(document.querySelector('#distchart'), distOptions);
    this.distchart.render();
  }
  distChange(event) {
    if (this.distchart !== undefined) {
      this.distchart.destroy();
    }
    this.distValue = [];
    this.distLabel = [];
    if (event === '1') {
      data.distribution_chart1.forEach(obj => {
        this.distValue.push(parseInt(obj.Sum_of_AUM, 10));
        this.distLabel.push(obj.Row_Labels);
      });
    } else if (event === '2') {
      data.distribution_chart2.forEach(obj => {
        this.distValue.push(parseInt(obj.Sum_of_AUM, 10));
        this.distLabel.push(obj.Row_Labels);
      });
    } else if (event === '3') {
      data.distribution_chart3.forEach(obj => {
        this.distValue.push(parseInt(obj.Sum_of_AUM, 10));
        this.distLabel.push(obj.Row_Labels);
      });
    } else if (event === '4') {
      data.distribution_chart4.forEach(obj => {
        this.distValue.push(parseInt(obj.Sum_of_AUM, 10));
        this.distLabel.push(obj.Row_Labels);
      });
    }
    this.distributionChart();
  }
  // --------------------- Donut Chart End ----------------------

  // --------------------- Area chart Income Start -----------------
  incomeTrendChart() {
    const incomeoptions = {
      chart: {
        toolbar: {
          show: false
        },
        height: 260,
        type: 'area',
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      series: [{
        name: 'Income Trend',
        data: this.incomeValue
      }],
      labels: this.incomeLabel,
      xaxis: {
        categories: this.incomeLabel,
      },
      yaxis: {
        opposite: false,
        labels: {
          formatter(value) {
            return (value / 1000000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
        },
      },
      legend: {
        horizontalAlign: 'left'
      }
    };
    this.incomeChart = new ApexCharts(
        document.querySelector('#incomeTrend'), incomeoptions
    );
    this.incomeChart.render();
  }
  incomeChange(event) {
    if (this.incomeChart !== undefined) {
      this.incomeChart.destroy();
    }
    this.incomeValue = [];
    this.incomeLabel = [];
    if (event === '1') {
      data.income_trends1.forEach(obj => {
        this.incomeValue.push(parseInt(obj.AUM, 10));
        this.incomeLabel.push(obj.Period);
      });
    } else if (event === '2') {
      data.income_trends2.forEach(obj => {
        this.incomeValue.push(parseInt(obj.AUM, 10));
        this.incomeLabel.push(obj.Period);
      });
    }
    this.incomeTrendChart();
  }
  // --------------------- Area chart Income End -----------------

  // --------------------- Donut  Chart Start --------------------
  incomeDistChart() {
    const incomeDistOptions = {
      labels: this.incomeDistLabel,
      dataLabels: {
        enabled: false
      },
      chart: {
        type: 'donut',
        height: 300
      },
      series: this.incomeDistValue,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };
    this.incomeDistchart = new ApexCharts(document.querySelector('#incomeDistchart'), incomeDistOptions);
    this.incomeDistchart.render();
  }
  incomeDistChange(event) {
    if (this.incomeDistchart !== undefined) {
      this.incomeDistchart.destroy();
    }
    this.incomeDistValue = [];
    this.incomeDistLabel = [];
    if (event === '1') {
      data.income_distribution1.forEach(obj => {
        this.incomeDistValue.push(parseInt(obj.Sum_Revenue, 10));
        this.incomeDistLabel.push(obj.Row_Labels);
      });
    } else if (event === '2') {
      data.income_distribution2.forEach(obj => {
        this.incomeDistValue.push(parseInt(obj.Sum_Revenue, 10));
        this.incomeDistLabel.push(obj.Row_Labels);
      });
    } else if (event === '3') {
      data.income_distribution3.forEach(obj => {
        this.incomeDistValue.push(parseInt(obj.Sum_Revenue, 10));
        this.incomeDistLabel.push(obj.Row_Labels);
      });
    } else if (event === '4') {
      data.income_distribution4.forEach(obj => {
        this.incomeDistValue.push(parseInt(obj.Sum_Revenue, 10));
        this.incomeDistLabel.push(obj.Row_Labels);
      });
    }
    this.incomeDistChart();
  }
  // --------------------- Donut Chart End ----------------------

  // ------------------  Expected Business Charts ----------------------
  // Area Chart
  investmentChart() {
    const investmentOptions = {
      chart: {
        toolbar: {
          show: false
        },
        height: 280,
        type: 'area',
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      series: [{
        name: 'Investment',
        data: this.aumValue
      }],
      labels: this.aumLabel,
      xaxis: {
        categories: this.aumLabel,
      },
      yaxis: {
        opposite: false,
        labels: {
          formatter(value) {
            return (value / 1000000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
        },
      },
      legend: {
        horizontalAlign: 'left'
      }
    };
    this.investChart = new ApexCharts(document.querySelector('#invest'), investmentOptions);
    this.investChart.render();
  }
  investChange() {
    if (this.investChart !== undefined) {
      this.investChart.destroy();
    }
    this.investValue = [];
    this.investLabel = [];
    data.invest_amount.forEach(obj => {
      this.investValue.push(parseInt(obj.AUM, 10));
      this.investLabel.push(obj.Period);
    });
    this.investmentChart();
  }
  // Donut Chart
  valueChart() {
    const valueOptions = {
      labels: this.valueLabel,
      dataLabels: {
        enabled: false
      },
      chart: {
        type: 'donut',
      },
      series: this.valueValue,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 150
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };
    this.investValueChart = new ApexCharts(document.querySelector('#valuechart'), valueOptions);
    this.investValueChart.render();
  }
  valueChartChange() {
    if (this.investValueChart !== undefined) {
      this.investValueChart.destroy();
    }
    this.valueValue = [];
    this.valueLabel = [];
    data.invest_value.forEach(obj => {
      this.valueValue.push(parseInt(obj.Value, 10));
      this.valueLabel.push(obj.Row_Labels);
    });
    this.valueChart();
  }
  // Pie Chart
  customerCount() {
    const options = {
      chart: {
        width: 450,
        height: 333,
        type: 'pie',
      },
      labels: this.customerCountLabel,
      series: this.customerCountValue,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };
    this.customerCountChart = new ApexCharts(document.querySelector('#customerCount'), options);
    this.customerCountChart.render();
  }
  countChange() {
    if (this.customerCountChart !== undefined) {
      this.customerCountChart.destroy();
    }
    this.customerCountValue = [];
    this.customerCountLabel = [];
    data.customer_count.forEach(obj => {
      this.customerCountValue.push(parseInt(obj.Count, 10));
      this.customerCountLabel.push(obj.Row_Labels);
    });
    this.customerCount();
  }
  // Bar Chart
  conversionChart() {
    const convOptions = {
      chart: {
        toolbar: {
          show: false
        },
        height: 280,
        type: 'bar',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%'
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [{
        name: 'Count',
        data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
      }],
      xaxis: {
        categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
      },
      yaxis: {
        title: {
          text: ''
        }
      },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter(val) {
            return val;
          }
        }
      }
    };
    this.convChart = new ApexCharts(document.querySelector('#convChart'), convOptions);
    this.convChart.render();
  }
  convChange() {
    if (this.convChart !== undefined) {
      this.convChart.destroy();
    }
    this.convValue = [];
    this.convLabel = [];
    data.conversion.forEach(obj => {
      this.convValue.push(parseInt(obj.Amount, 10));
      this.convLabel.push(obj.Row_Labels);
    });
    this.conversionChart();
  }
}
