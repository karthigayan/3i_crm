import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildOverviewChartComponent } from './child-overview-chart.component';

describe('ChildOverviewChartComponent', () => {
  let component: ChildOverviewChartComponent;
  let fixture: ComponentFixture<ChildOverviewChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildOverviewChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildOverviewChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
