import {Component, NgZone, OnInit} from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { ChartType, ChartEvent } from 'ng-chartist/dist/chartist.component';
import * as Chartist from 'chartist';
// @ts-ignore
import ApexCharts from 'apexcharts';
import {CustomerDetailService} from '../../../_service/customer/customer-detail.service';
declare var require: any;

/*------ AmChart ---------------------------*/
const data: any = require('../your-performance/data.json');
const data1: any = require('./data.json');
export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-child-overview-chart',
  templateUrl: './child-overview-chart.component.html',
  styleUrls: ['./child-overview-chart.component.css']
})
export class ChildOverviewChartComponent implements  OnInit {
  public pieChartValue1: any;
  public pieChartLabel1: any;
  exposureChart;
  public customerId1: any;
  public selectedCustomer1: any;
  public customerRisk: any;
  public performValue: any;
  public areaStackChart: any;
  public areaStackValueIv: any;
  public areaStackValueCv: any;
  public areaStackValueBv: any;
  public areaStackLabel: any;
  public portfolioValue: any;


  constructor(private zone: NgZone,
              private custId: CustomerDetailService) {
    this.customerId1 = this.custId.getMyCustomer();
    console.log('test' + this.customerId1);

    this.selectedCustomer1 = data1.customer_details.filter( val => parseInt(val.Customer_ID, 10) === this.customerId1);
    this.customerRisk = data1.customer_risk.filter( val => parseInt(val.Customer_ID, 10) === this.customerId1);

    this.performValue = data1.portfolio_value.filter( item => parseInt(item.Customer_ID, 10) === this.customerId1 );
    console.log(this.performValue);
  }
// -------------------------  Chart Start --------------------------------

  ngOnInit() {
    this.fnExposureChange('Asset Class');
    this.areaPortfolioChange('monthly');

  }

  // ----------- Customer Details and  Chart -------------
  fnExposureChange(value) {
    if (this.exposureChart !== undefined) {
      this.exposureChart.destroy();
    }

    if (value === 'Asset Class') {
        this.pieChartValue1 = [];
        this.pieChartLabel1 = [];
      // tslint:disable-next-line:radix
        this.pieChartValue1 = [parseInt(this.selectedCustomer1[0].Equity),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Fixed_Income_Allocation),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Alternate_Alloation),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Cash_Allocation)];
        this.pieChartLabel1 = ['Equity', 'Fixed Income Allocation', 'Alternate Alloation', 'Cash Allocation'];

    } else if (value === 'Sub Asset Class') {
        this.pieChartValue1 = [];
        this.pieChartLabel1 = [];
      // tslint:disable-next-line:radix radix
        this.pieChartValue1 = [parseInt(this.selectedCustomer1[0].Direct_Equity),
          // tslint:disable-next-line:radix radix
          parseInt(this.selectedCustomer1[0].Equity_Mutual_Fund),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Debt_Mutual_Fund),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Alternate_Investment_Fund),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Money_Market_Fund),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Real_Estate),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Bonds),
          // tslint:disable-next-line:radix
          parseInt(this.selectedCustomer1[0].Cash)];
        this.pieChartLabel1 = ['Direct Equity', 'Equity Mutual Fund', 'Debt Mutual Fund',
          'Alternate Investment Fund', 'Money Market Fund', 'Real Estate', 'Bonds', 'Cash'];
    }
    this.exposureDefaultChart();
  }

  exposureDefaultChart() {
    const options = {
      chart: {
        width: 400,
        type: 'pie',
      },
      labels:  this.pieChartLabel1,
      series:  this.pieChartValue1,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    this.exposureChart = new ApexCharts(
        document.querySelector('#exposureChart'),
        options
    );

    this.exposureChart.render();
  }
// --------------------  Protfolio Performance -----------------------
  areaPortfolio() {
    const options = {
      chart: {
        height: 350,
        type: 'area',
        stacked: true,
        events: {
          selection(chart, e) {
            console.log(new Date(e.xaxis.min) );
          }
        },

      },
      colors: ['#008FFB', '#00E396', '#CED4DC'],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },

      series: [{
        name: 'Investment Value',
        data: this.areaStackValueIv
      },
        {
          name: 'Capital Invested ',
          data: this.areaStackValueCv
        },
        {
          name: 'Benchmark Value',
          data: this.areaStackValueBv
        }
      ],
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 0.6,
          opacityTo: 0.8,
        }
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left'
      },
      xaxis: {
        categories: this.areaStackLabel
      },
    };

    this.areaStackChart = new ApexCharts(
        document.querySelector('#areaStackChart'),
        options
    );
    this.areaStackChart.render();
  }
  areaPortfolioChange(event) {
    if (this.areaStackChart !== undefined) {
      this.areaStackChart.destroy();
    }
    this.areaStackValueIv = [];
    this.areaStackValueCv = [];
    this.areaStackValueBv = [];
    this.areaStackLabel = [];
    if (event === 'monthly') {
      this.portfolioValue = this.performValue.filter( val => val.Period === 'Monthly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Investment_Value)
        );
        this.areaStackValueCv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Capital_Invested)
        );
        this.areaStackValueBv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Benchmark_Value)
        );
        this.areaStackLabel.push(
            obj.Data_Value
        );
      });

    } else if (event === 'quarterly') {
      this.portfolioValue = this.performValue.filter( val => val.Period === 'Quarterly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Investment_Value)
        );
        this.areaStackValueCv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Capital_Invested)
        );
        this.areaStackValueBv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Benchmark_Value)
        );
        this.areaStackLabel.push(
            obj.Data_Value
        );
      });
    } else if (event === 'yearly') {
      this.portfolioValue = this.performValue.filter( val => val.Period === 'Yearly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Investment_Value)
        );
        this.areaStackValueCv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Capital_Invested)
        );
        this.areaStackValueBv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Benchmark_Value)
        );
        this.areaStackLabel.push(
            obj.Data_Value
        );
      });
    }
    this.areaPortfolio();
  }



}
