import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './_guards/auth.guard';
import {FullComponent} from './_custom_components/pages/full/full.component';
import {BlankComponent} from './_custom_components/pages/blank/blank.component';
import {ChildComponent} from './_custom_components/pages/child/child.component';

export const Approutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        loadChildren: () => import('./_modules/dashboard/dashboard.module').then(
            m => m.DashboardModule
        )
      },
      {
        path: 'customers',
        loadChildren: () => import('./_modules/customer/customer.module').then(
            m => m.CustomerModule
        )
      },
      {
        path: 'prospect',
        loadChildren: () => import('./_modules/prospect/prospect.module').then(
            m => m.ProspectModule
        )
      },
      {
        path: 'potentials',
        loadChildren: () => import('./_modules/potentials/potentials.module').then(
            m => m.PotentialsModule
        )
      },
      {
        path: 'profileDetails',
        loadChildren: () => import('./_modules/profile/profile.module').then(
            m => m.ProfileModule
        )
      },
      {
        path: 'engagement',
        loadChildren: () => import ('./_modules/engagement/engagement.module').then(
          m => m.EngagementModule
        )

      }
    ]
  },
  {
    path: '',
    component: BlankComponent,
    children: [
      {
        path: 'authentication',
        loadChildren: () => import('./_modules/auth/auth.module').then(
            m => m.AuthModule
        )
      }
    ]
  },
  {
    path: '',
    component: ChildComponent,
    children: [
      {
        path: 'overview',
        loadChildren: () => import('./_modules/childApp/overview/overview.module').then(
            m => m.OverviewModule
        )
      }
    ]
  }
];

