import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerDetailService {

  myCustomerId: any;
  constructor() {
    this.myCustomerId = 6477835;
  }

  setMyCustomer(val: any) {
    this.myCustomerId = val;
  }

  getMyCustomer() {
    return this.myCustomerId;
  }
}
