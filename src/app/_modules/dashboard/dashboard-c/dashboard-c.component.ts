import {Component, OnInit} from '@angular/core';
import dashboard from './data/dashboard.json';
import {currency} from '../../../_config/currency';

@Component({
  selector: 'app-dashboard-c',
  templateUrl: './dashboard-c.component.html',
  styleUrls: ['./dashboard-c.component.css']
})
export class DashboardCComponent implements OnInit {
  gaugemap = {
    configure: undefined
  };
  public topBand = dashboard.top_band[0];
  public performance = dashboard.performance;
  public top_customer = dashboard.top_customer;
  public key_potential = dashboard.key_potential;
  public recent_prospect = dashboard.recent_prospect;
  public performanceChart = [];
  public gaugeType: any;
  public gaugeValue: any;
  public gaugeLabel: any;
  public gaugeAppendText: any;
  public gaugemax: any;
  public thresholdConfig: any;
  public gaugeType2: any;
  public gaugeValue2: any;
  public gaugeLabel2: any;
  public gaugeAppendText2: any;
  public gaugemax2: any;
  public thresholdConfig2: any;
  public gaugeType3: any;
  public gaugeValue3: any;
  public gaugeLabel3: any;
  public gaugeAppendText3: any;
  public gaugemax3: any;
  public thresholdConfig3: any;
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  constructor() { }
  // -----------------  Gauge Start ---------------------

  ngOnInit() {
    this.selectPerform('month');
  }

  selectPerform(itemValue) {
    // chart 1
    if (itemValue === 'month') {
      this.performanceChart = this.performance.filter( val => val.Period === 'Monthly' );
      // tslint:disable-next-line:radix
      this.gaugeValue = parseInt(this.performanceChart[0].Completion);
      // tslint:disable-next-line:radix
      this.gaugeValue2 = parseInt(this.performanceChart[1].Completion);
      // tslint:disable-next-line:radix
      this.gaugeValue3 = parseInt(this.performanceChart[2].Completion);
      // tslint:disable-next-line:radix
      this.gaugeLabel = parseInt(this.performanceChart[0].Actual);
      // tslint:disable-next-line:radix
      this.gaugeLabel2 = parseInt(this.performanceChart[1].Actual);
      // tslint:disable-next-line:radix
      this.gaugeLabel3 = parseInt(this.performanceChart[2].Actual);
    } else if (itemValue === 'quarterly') {
      this.performanceChart = this.performance.filter( val => val.Period === 'Qurtery' );
      // tslint:disable-next-line:radix
      this.gaugeValue = parseInt(this.performanceChart[0].Completion);
      // tslint:disable-next-line:radix
      this.gaugeValue2 = parseInt(this.performanceChart[1].Completion);
      // tslint:disable-next-line:radix
      this.gaugeValue3 = parseInt(this.performanceChart[2].Completion);
      // tslint:disable-next-line:radix
      this.gaugeLabel = parseInt(this.performanceChart[0].Actual);
      // tslint:disable-next-line:radix
      this.gaugeLabel2 = parseInt(this.performanceChart[1].Actual);
      // tslint:disable-next-line:radix
      this.gaugeLabel3 = parseInt(this.performanceChart[2].Actual);
    } else if (itemValue === 'annually') {
      this.performanceChart = this.performance.filter( val => val.Period === 'Annually' );
      // tslint:disable-next-line:radix
      this.gaugeValue = parseInt(this.performanceChart[0].Completion);
      // tslint:disable-next-line:radix
      this.gaugeValue2 = parseInt(this.performanceChart[1].Completion);
      // tslint:disable-next-line:radix
      this.gaugeValue3 = parseInt(this.performanceChart[2].Completion);
      // tslint:disable-next-line:radix
      this.gaugeLabel = parseInt(this.performanceChart[0].Actual);
      // tslint:disable-next-line:radix
      this.gaugeLabel2 = parseInt(this.performanceChart[1].Actual);
      // tslint:disable-next-line:radix
      this.gaugeLabel3 = parseInt(this.performanceChart[2].Actual);
    }
    this.gaugeType = 'semi';
    this.gaugeAppendText = '%';
    this.gaugemax = 100;
    this.thresholdConfig = {
      0: {color: 'rgb(231, 76, 60 )'},
      40: {color: 'rgb(245, 176, 65)'},
      75: {color: 'rgb(46, 204, 113)'}
    };
    // chart 2
    this.gaugeType2 = 'semi';
    this.gaugeAppendText2 = '%';
    this.gaugemax2 = 100;
    this.thresholdConfig2 = {
      0: {color: 'rgb(231, 76, 60 )'},
      40: {color: 'rgb(245, 176, 65)'},
      75: {color: 'rgb(46, 204, 113)'}
    };

    // chart 3
    this.gaugeType3 = 'semi';
    this.gaugeAppendText3 = '%';
    this.gaugemax3 = 100;
    this.thresholdConfig3 = {
      0: {color: 'rgb(231, 76, 60 )'},
      40: {color: 'rgb(245, 176, 65)'},
      75: {color: 'rgb(46, 204, 113)'}
    };
    // -----------------  Gauge End ---------------------
  }

}
