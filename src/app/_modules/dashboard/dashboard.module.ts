import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardCComponent } from './dashboard-c/dashboard-c.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {MaterialModuleModule} from '../../_custom_components/material-module/material-module.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxGaugeModule} from 'ngx-gauge';
import {ChartModule} from '../../_custom_components/charts/charts.module';


@NgModule({
  declarations: [DashboardCComponent ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        MDBBootstrapModule.forRoot(),
        MaterialModuleModule,
        FlexLayoutModule,
        NgxGaugeModule,
        ChartModule
    ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class DashboardModule {}
