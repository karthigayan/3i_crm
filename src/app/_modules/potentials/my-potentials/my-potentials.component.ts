import { Component, OnInit } from '@angular/core';
import data from './data.json';
import {Router} from '@angular/router';
import {LabelType, Options} from 'ng5-slider';
import {currency} from '../../../_config/currency';
// @ts-ignore
import ApexCharts from 'apexcharts';

@Component({
  selector: 'app-my-potentials',
  templateUrl: './my-potentials.component.html',
  styleUrls: ['./my-potentials.component.css']
})
export class MyPotentialsComponent implements OnInit {
  potential: any = data;
  public result: any;
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  // ------------ Range Slider ----------
  // one
  value = 0;
  highValue = 18000000;
  options: Options = {
    floor: 0,
    ceil: 50000000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        case LabelType.High:
          return  '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        default:
          return '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      }
    }
  };

  // --------------- Filter ------------------
  filter = { prospect: false, new: false, investment_product : false, fiduciary_services: false,
    execution_only_account: false, ima: false, pms: false,
    introduction: false, qualification: false, need_analysis: false, advanced_discussion: false,
    final_evaluation: false

  };
  opportunityList = [];
  opportunityItems = [];
  opportunitySettings = {};
  stageList = [];
  stageItems = [];
  stageSettings = {};
  constructor(private router: Router) {}

  ngOnInit() {
    this.result = this.potential;
    // --------------- Multi-select ---------------------
    // Opportunity
    this.opportunityList = [
      { item_id: 'investment_product', item_text: 'Investment Product' },
      { item_id: 'fiduciary_services', item_text: 'Fiduciary Services' },
      { item_id: 'execution_only_account', item_text: 'Execution Only Account' },
      { item_id: 'ima', item_text: 'Investment Management Account' },
      { item_id: 'pms', item_text: 'Portfolio Management Services' }
    ];
    this.opportunityItems = [];
    this.opportunitySettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // Stage
    this.stageList = [
      { item_id: 'introduction', item_text: 'Introduction' },
      { item_id: 'qualification', item_text: 'Qualification' },
      { item_id: 'need_analysis', item_text: 'Need Analysis' },
      { item_id: 'advanced_discussion', item_text: 'Advanced Discussion' },
      { item_id: 'final_evaluation', item_text: 'Final Evaluation' }
    ];
    this.stageItems = [];
    this.stageSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    this.Custfilter();

    const spark1 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 50,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        // colors: ['#f2072b']
      },
      series: [{
        data: [90, 20, 50, 38, 56, 24, 65]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        min: 0
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const spark2 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 50,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        // colors: ['#1ef207']
      },
      series: [{
        data: [47, 45, 54, 38, 56, 24, 65]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        min: 0
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };

    new ApexCharts(document.querySelector('#sparkChart'), spark1).render();
    new ApexCharts(document.querySelector('#sparkChart1'), spark2).render();
  }
  onItemSelect(item: any) {
    this.filter[item.item_id] = true;
    this.Custfilter();
    console.log(this.filter);
  }
  onItemDeSelect(deitem: any) {
    this.filter[deitem.item_id] = false;
    this.Custfilter();
    console.log(this.filter);
  }
  onNavigate() {
    this.router.navigate(['']);
  }

  Custfilter() {
    console.log( this.highValue);
    // -----------------------  Range Slider Filter -----------------------------
    this.result = data.filter(d => (parseInt(d.Target_Investment, 10) >
        this.value && parseInt(d.Target_Investment, 10) <  this.highValue));
    // ----------------------- Risk Profile Filter ---------------------------

    if (this.filter.prospect || this.filter.new ) {
      this.result = this.result.filter(d => (d.Type === 'Prospect' && this.filter.prospect || d.Type === 'New' && this.filter.new)
      );
    }
    // ----------------------- Category ---------------------------
    if (this.filter.investment_product || this.filter.fiduciary_services || this.filter.execution_only_account
        || this.filter.ima || this.filter.pms) {
      this.result = this.result.filter(d => (d.Oppurtunity === 'Investment Product' && this.filter.investment_product)
          || (d.Oppurtunity === 'Fiduciary Services' && this.filter.fiduciary_services)
          || (d.Oppurtunity === 'Execution Only Account' && this.filter.execution_only_account)
          || (d.Oppurtunity === 'Investment Management Account' && this.filter.ima)
          || (d.Oppurtunity === 'Portfolio Management Services' && this.filter.pms)
      );
    }
    // ----------------------- Source ---------------------------
    if (this.filter.introduction || this.filter.qualification || this.filter.need_analysis || this.filter.advanced_discussion
        || this.filter.final_evaluation) {
      this.result = this.result.filter(d => (d.Stage === 'Introduction' && this.filter.introduction)
          || (d.Stage === 'Qulification' && this.filter.qualification)
          || (d.Stage === 'Need Analysis' && this.filter.need_analysis)
          || (d.Stage === 'Advanced Discussion' && this.filter.advanced_discussion)
          || (d.Stage === 'Final Evaluation' && this.filter.final_evaluation));
    }

  }
}
