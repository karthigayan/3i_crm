import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPotentialsComponent } from './my-potentials.component';

describe('MyPotentialsComponent', () => {
  let component: MyPotentialsComponent;
  let fixture: ComponentFixture<MyPotentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPotentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPotentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
