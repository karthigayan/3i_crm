import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PotentialsOverviewComponent } from './potentials-overview.component';

describe('PotentialsOverviewComponent', () => {
  let component: PotentialsOverviewComponent;
  let fixture: ComponentFixture<PotentialsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotentialsOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PotentialsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
