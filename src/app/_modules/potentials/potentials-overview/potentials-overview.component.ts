import { Component, OnInit } from '@angular/core';
import {currency} from '../../../_config/currency';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist/dist/chartist.component';
// @ts-ignore
import ApexCharts from 'apexcharts';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

declare var require: any;

const data: any = require('./data.json');

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-potentials-overview',
  templateUrl: './potentials-overview.component.html',
  styleUrls: ['./potentials-overview.component.css']
})
export class PotentialsOverviewComponent implements OnInit {
  breakup: any;
  expectedConversion: any;
  conversionTrend: any;
  doughnutView: any;
  chartColorsStage: any;
  doughnutChartTypeStage: string;
  doughnutChartStageData: any;
  doughnutChartStageLabels: any;
  doughnutChartTypeLabels: any;
  doughnutChartTypeData: any;
  chartColorsType: any;
  // ----------------- Near Close ----------------
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  public nearCloserValue: any;
  // -----Spark Line -----------------
  public data1: any[] = [
    936, 968, 1025, 999, 998, 1014, 1017, 1010, 1010, 1007
  ];
  // ----- Funnel chat ------------
  public model;
  // Barchart
  barChart1: Chart;
  public doughnutChartType: any;
  public bubbleData: any;
  highValue: any;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.highValue = data.high_value;
    this.model = data.salesPipeline;
    this.doughnutType();
    this.doughnutStage();
    this.loadPotentialBreakdown();
    this.doughnutView = 'Type';
    this.loadTrend();
    this.loadExpectedConversion();
    // --------------------  SparkLine Bar Chart Start ----------------------------
    const spark1 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 50,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        // colors: ['#f2072b']
      },
      series: [{
        data: [90, 20, 50, 38, 56, 24, 65]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        min: 0
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const spark2 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 50,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        // colors: ['#1ef207']
      },
      series: [{
        data: [47, 45, 54, 38, 56, 24, 65]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        min: 0
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const options3 = {
      chart: {
        type: 'bar',
        width: 100,
        height: 50,
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [50, 50, 80, 10, 20, 25, 44, 12, 50, 90, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      },
      fill: {
        colors: ['#e827e5']
      }
    };
    const options4 = {
      chart: {
        type: 'bar',
        width: 100,
        height: 50,
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      },
      fill: {
        colors: ['#27a4e8']
      },
    };
    const options5 = {
      chart: {
        type: 'bar',
        width: 100,
        height: 50,
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      },
      fill: {
        colors: ['#e2e617']
      },
    };
    const options6 = {
      chart: {
        type: 'bar',
        width: 100,
        height: 50,
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      },
      fill: {
        colors: ['#F44336']
      },
    };
    const options7 = {
      chart: {
        type: 'bar',
        width: 100,
        height: 50,
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      },
      fill: {
        colors: ['#f90']
      },
    };

    new ApexCharts(document.querySelector('#sparkChart'), spark1).render();
    new ApexCharts(document.querySelector('#sparkChart1'), spark2).render();
    new ApexCharts(document.querySelector('#sparkChart2'), options3).render();
    new ApexCharts(document.querySelector('#sparkChart3'), options4).render();
    new ApexCharts(document.querySelector('#sparkChart4'), options5).render();
    new ApexCharts(document.querySelector('#sparkChart5'), options6).render();
    new ApexCharts(document.querySelector('#sparkChart7'), options7).render();
    // -----------------------  Spark Line Chart End ----------------------------------
    this.barChart();
    this.bubbleChart();
  }

// ----------------------  Vertical Grouped Bar Chart ------------------
  barChart() {
    const barChartA = {
      chart: {
        height: 310,
        type: 'bar',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          dataLabels: {
            position: 'top',
          },
        }
      },
      dataLabels: {
        enabled: false,
        offsetX: -6,
        style: {
          fontSize: '12px',
          colors: ['#fff']
        }
      },
      stroke: {
        show: true,
        width: 1,
        colors: ['#fff']
      },
      series: [{
        name: 'Client',
        data: [44, 55, 41, 64, 22, 43, 21]
      }, {
        name: 'Prospect',
        data: [53, 32, 33, 52, 13, 44, 32]
      }],
      xaxis: {
        categories: [2001, 2002, 2003, 2004, 2005, 2006, 2007],
      },

    };


    const chart = new ApexCharts(
        document.querySelector('#barChart'),
        barChartA
    );

    chart.render();
  }
  // --------------------  Bubble Chart -------------------------------
  bubbleChart() {
    // ---------------------- Bubble Chart -------------------------
    this.bubbleData = [];
    data.potential_aging.forEach(obj => {
      this.bubbleData.push({
        // tslint:disable-next-line:radix
        name: parseInt(obj.Probability),
        data: [
          // tslint:disable-next-line:radix
          [parseInt(obj.Age ), parseInt(obj.Probability ), parseInt(obj.Target_Investment_Amount)]
        ]
      });
    });
    const bubbleOptions = {

      chart: {
        height: 320,
        type: 'bubble'

      },
      dataLabels: {
        enabled: false
      },
      legend: {
        show: false
      },
      series: this.bubbleData,
      fill: {
        opacity: 0.8,
      },
      title: {
        text: ''
      },
      xaxis: {
        min: 0,
        max: 1000
      },
      yaxis: {
        min: 0,
        max: 100
      }
    };

    const bubblechart = new ApexCharts(
        document.querySelector('#Bubblechart'),
        bubbleOptions
    );

    bubblechart.render();
  }
  loadExpectedConversion() {
    const expectedConversionOptions = {

      chart: {
        height: 300,
        type: 'line',
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      series: [{
        name: 'Conversion Trend',
        data: data.trendCreation.value
      }],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      title: {
        text: '',
        align: 'left'
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {
        categories: data.trendCreation.lable,
      }
    };

    this.conversionTrend = new ApexCharts(
        document.querySelector('#conversionTrend'),
        expectedConversionOptions
    );
    this.conversionTrend.render();
  }
  loadTrend() {
    const trendOptions = {
      chart: {
        height: 300,
        type: 'line',
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      series: [{
        name: 'Expected Conversion',
        data: data.trendClosure.value
      }],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      title: {
        text: '',
        align: 'left'
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {
        categories: data.trendClosure.lable,
      }
    };

    this.expectedConversion = new ApexCharts(
        document.querySelector('#expectedConversion'),
        trendOptions
    );
    this.expectedConversion.render();
    // new ApexCharts(document.querySelector('#conversionTrend'), options).render();
  }
  loadPotentialBreakdown() {
    this.barChart1 = {
      type: 'Bar',
      data: data.BarType,
      options: {
        seriesBarDistance: 15,
        height: 300,
        axisX: {
          showGrid: false,
          offset: 70
        },
        axisY: {
          showGrid: true,
          offset: 50
        }
      },
      responsiveOptions: [
        [
          'screen and (min-width: 640px)',
          {
            axisX: {
              labelInterpolationFnc(value: number, index: number): string {
                return index % 1 === 0 ? `${value}` : null;
              }
            }
          }
        ]
      ]
    };
  }

  doughnutType() {
    this.doughnutChartTypeLabels = data.doughnutChartType.doughnutChartLabels;
    this.doughnutChartTypeData = data.doughnutChartType.doughnutChartData;
    this.doughnutChartType = 'doughnut';
    this.chartColorsType = data.doughnutChartColor;
  }
  doughnutStage() {
    this.doughnutChartStageLabels = data.doughnutChartStage.doughnutChartLabels;
    this.doughnutChartStageData = data.doughnutChartStage.doughnutChartData;
    this.doughnutChartTypeStage = 'doughnut';
    this.chartColorsStage = data.doughnutChartColor;
  }
  fnBreakupChange(value) {

    if (value === 'Type') {
      this.breakup = data.BarType;
    } else if (value === 'Stage') {
      this.breakup = data.BarStage;
    }
    this.fnBarCharData();
  }
  fnBarCharData() {
    this.barChart1 = {
      type: 'Bar',
      data: this.breakup,
      options: {
        seriesBarDistance: 15,
        height: 400,
        axisX: {
          showGrid: false,
          offset: 70
        },
        axisY: {
          showGrid: true,
          offset: 50
        }
      },
      responsiveOptions: [
        [
          'screen and (min-width: 640px)',
          {
            axisX: {
              labelInterpolationFnc(value: number, index: number): string {
                return index % 1 === 0 ? `${value}` : null;
              }
            }
          }
        ]
      ]
    };
  }
  fnDistributionChange(value) {
    if (value === 'Type') {
      this.doughnutView = 'Type';
    } else if (value === 'Stage') {
      this.doughnutView = 'Stage';
    }

  }
  // ---------------------- Closer Modal -----------------
  closerModal(content, item) {
    if (item === 'near_closer') {
      this.nearCloserValue = data.near_closer;
    } else if (item === 'due_now') {
      this.nearCloserValue = data.due_now;
    } else if (item === 'missed_due') {
      this.nearCloserValue = data.missed_due;
    } else if (item === 'moved_up') {
      this.nearCloserValue = data.moved_up;
    } else if (item === 'moved_down') {
      this.nearCloserValue = data.moved_down;
    }

    this.modalService.open(content, {
      size: 'lg',
      centered: true});
  }
  highValueMore(content1) {
    this.modalService.open(content1, {
      size: 'lg',
      centered: true});
  }
}
