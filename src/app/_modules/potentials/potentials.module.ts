import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PotentialsRoutingModule } from './potentials-routing.module';
import { PotentialsOverviewComponent } from './potentials-overview/potentials-overview.component';
import { MyPotentialsComponent } from './my-potentials/my-potentials.component';
import {SparklineModule} from '@progress/kendo-angular-charts';
import {ChartistModule} from 'ng-chartist';
import {ChartsModule} from 'ng2-charts';
import {FormsModule} from '@angular/forms';
import {Ng5SliderModule} from 'ng5-slider';
import {MaterialModuleModule} from '../../_custom_components/material-module/material-module.module';
import {NgCircleProgressModule} from 'ng-circle-progress';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [PotentialsOverviewComponent, MyPotentialsComponent],
  imports: [
    CommonModule,
    PotentialsRoutingModule,
    SparklineModule,
    ChartistModule,
    ChartsModule,
    FormsModule,
    Ng5SliderModule,
    MaterialModuleModule,
    NgbModule,
    NgCircleProgressModule.forRoot({
      radius: 80,
      outerStrokeWidth: 4,
      innerStrokeWidth: 0,
      outerStrokeColor: '#78C000',
      innerStrokeColor: '#C7E596',
      animationDuration: 300
    }),
    NgMultiSelectDropDownModule
  ]
})
export class PotentialsModule { }
