import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PotentialsOverviewComponent} from './potentials-overview/potentials-overview.component';
import {MyPotentialsComponent} from './my-potentials/my-potentials.component';

const routes: Routes = [
  {path: '', children: [
      {path: '', component: PotentialsOverviewComponent},
      {path: 'mypotentials', component: MyPotentialsComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PotentialsRoutingModule { }
