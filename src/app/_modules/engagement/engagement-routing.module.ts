import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EngagementOverviewComponent} from './engagement-overview/engagement-overview.component';
import {MyActivitiesComponent} from './my-activities/my-activities.component';

const routes: Routes = [
  { path: '', children: [
    {path: '', component: EngagementOverviewComponent},
      {path: 'myactivities', component: MyActivitiesComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EngagementRoutingModule { }
