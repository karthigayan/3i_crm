import { Component, OnInit } from '@angular/core';
import data from './data.json';
// @ts-ignore
import ApexCharts from 'apexcharts';

@Component({
  selector: 'app-my-activities',
  templateUrl: './my-activities.component.html',
  styleUrls: ['./my-activities.component.css']
})
export class MyActivitiesComponent implements OnInit {
  public activity = data.activity;
  constructor() { }

  ngOnInit() {
    this.topbar1();
    this.topbar2();
    this.topbar3();
  }

  topbar1() {
    var options = {
      chart: {
        width: 200,
        height: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          dataLabels: {
            name: {
              fontSize: '10px',
            }, value: {
              show: true,
              fontSize: '10px',
              fontFamily: undefined,
              color: undefined,
              offsetY: 16,
              formatter: function (val) {
                return val
              }
            },
            total: {
              show: false,
              label: 'Total',
              formatter: function (w) {
                // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                return 249
              }
            }
          }
        }
      },
      series: data.topBar1.values,
      labels: data.topBar1.labels,

    }

    var topBar1 = new ApexCharts(
        document.querySelector("#topBar1"),
        options
    );

    topBar1.render();
  }
  topbar2() {
    var options = {
      chart: {
        width: 200,
        height: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          offsetY: -10,
          startAngle: 0,
          endAngle: 270,
          hollow: {
            margin: 5,
            size: '30%',
            background: 'transparent',
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,

            },
            value: {
              show: false,
            }
          }
        }
      },
      colors: ['#1ab7ea', '#0084ff', '#39539E', '#0077B5'],
      series: data.topBar2.values,
      labels: data.topBar2.labels,
      legend: {
        show: false,
        floating: true,
        fontSize: '16px',
        position: 'left',
        offsetX: 160,
        offsetY: 10,
        labels: {
          useSeriesColors: true,
        },
        markers: {
          size: 0
        },
        formatter: function (seriesName, opts) {
          return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
        },
        itemMargin: {
          bottom: 1,
        }
      },
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            show: false
          }
        }
      }]
    }

    var topBar2 = new ApexCharts(
        document.querySelector("#topBar2"),
        options
    );

    topBar2.render();
  }
  topbar3() {
    var options = {
      chart: {
        width: 200,
        height: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          offsetY: -10,
          startAngle: 0,
          endAngle: 270,
          hollow: {
            margin: 5,
            size: '30%',
            background: 'transparent',
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,

            },
            value: {
              show: false,
            }
          }
        }
      },
      colors: ['#1ab7ea', '#0084ff', '#39539E', '#0077B5'],
      series: data.topBar3.values,
      labels: data.topBar3.labels,
      legend: {
        show: false,
        floating: true,
        fontSize: '16px',
        position: 'left',
        offsetX: 160,
        offsetY: 10,
        labels: {
          useSeriesColors: true,
        },
        markers: {
          size: 0
        },
        formatter: function (seriesName, opts) {
          return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
        },
        itemMargin: {
          bottom: 1,
        }
      },
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            show: false
          }
        }
      }]
    }

    var topBar3 = new ApexCharts(
        document.querySelector("#topBar3"),
        options
    );

    topBar3.render();
  }

}
