import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EngagementRoutingModule } from './engagement-routing.module';
import { EngagementOverviewComponent } from './engagement-overview/engagement-overview.component';
import { MyActivitiesComponent } from './my-activities/my-activities.component';
import {MaterialModuleModule} from '../../_custom_components/material-module/material-module.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [EngagementOverviewComponent, MyActivitiesComponent],
  imports: [
    CommonModule,
    EngagementRoutingModule,
    MaterialModuleModule,
    NgbModule,
  ]
})
export class EngagementModule { }
