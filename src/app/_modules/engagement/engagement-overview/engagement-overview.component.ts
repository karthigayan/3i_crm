import { Component, OnInit } from '@angular/core';
// @ts-ignore
import ApexCharts from 'apexcharts';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

declare var require: any;

const data: any = require('./data.json');
@Component({
  selector: 'app-engagement-overview',
  templateUrl: './engagement-overview.component.html',
  styleUrls: ['./engagement-overview.component.css']
})
export class EngagementOverviewComponent implements OnInit {
  ticketChat: any;
  fnTicketValue: any;
  activityBreakupChart: any;
  activityBreakupValue: any;
  engagementDistibutionValue: any;
  engagementDistibution: any;
  openActivities: any;
  constructor(private modalService: NgbModal) { }
  ngOnInit() {
    this.topbar1();
    this.topbar2();
    this.topbar3();
    this.engagementDistibutionValue = data.engagementDistributionStatus;
    this.defaultEngagementDistributionChart();
    this.enagementTrend();
    this.activityBreakupValue = data.activityBreakupType;
    this.activityBreakup();
    this.fnTicketValue = data.ticketBreakupType;
    this.ticketChartData();
    this.getOpenActivities();
  }
  getOpenActivities() {
    this.openActivities = data.openActivities;
    console.log(this.openActivities);
  }
  topbar1() {
    const options = {
      chart: {
        width: 200,
        height: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          dataLabels: {
            name: {
              fontSize: '10px',
            }, value: {
              show: true,
              fontSize: '10px',
              fontFamily: undefined,
              color: undefined,
              offsetY: 16,
              formatter(val) {
                return val;
              }
            },
            total: {
              show: false,
              label: 'Total',
              formatter(w) {
                return 249;
              }
            }
          }
        }
      },
      series: data.topBar1.values,
      labels: data.topBar1.labels,

    };

    const topBar1 = new ApexCharts(
      document.querySelector('#topBar1'),
      options
    );

    topBar1.render();
  }
  topbar2() {
    const options = {
      chart: {
        width: 200,
        height: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          offsetY: -10,
          startAngle: 0,
          endAngle: 270,
          hollow: {
            margin: 5,
            size: '30%',
            background: 'transparent',
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,

            },
            value: {
              show: false,
            }
          }
        }
      },
      colors: ['#1ab7ea', '#0084ff', '#39539E', '#0077B5'],
      series: data.topBar2.values,
      labels: data.topBar2.labels,
      legend: {
        show: false,
        floating: true,
        fontSize: '16px',
        position: 'left',
        offsetX: 160,
        offsetY: 10,
        labels: {
          useSeriesColors: true,
        },
        markers: {
          size: 0
        },
        formatter(seriesName, opts) {
          return seriesName + ':  ' + opts.w.globals.series[opts.seriesIndex];
        },
        itemMargin: {
          bottom: 1,
        }
      },
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            show: false
          }
        }
      }]
    };

    const topBar2 = new ApexCharts(
      document.querySelector('#topBar2'),
      options
    );

    topBar2.render();
  }
  topbar3() {
    const options = {
      chart: {
        width: 200,
        height: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          offsetY: -10,
          startAngle: 0,
          endAngle: 270,
          hollow: {
            margin: 5,
            size: '30%',
            background: 'transparent',
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,

            },
            value: {
              show: false,
            }
          }
        }
      },
      colors: ['#1ab7ea', '#0084ff', '#39539E', '#0077B5'],
      series: data.topBar3.values,
      labels: data.topBar3.labels,
      legend: {
        show: false,
        floating: true,
        fontSize: '16px',
        position: 'left',
        offsetX: 160,
        offsetY: 10,
        labels: {
          useSeriesColors: true,
        },
        markers: {
          size: 0
        },
        formatter(seriesName, opts) {
          return seriesName + ':  ' + opts.w.globals.series[opts.seriesIndex];
        },
        itemMargin: {
          bottom: 1,
        }
      },
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            show: false
          }
        }
      }]
    };

    const topBar3 = new ApexCharts(
      document.querySelector('#topBar3'),
      options
    );

    topBar3.render();
  }
  fnEngagementDistributionChange(value) {
    if (this.engagementDistibution !== undefined) {
      this.engagementDistibution.destroy();
    }
    this.engagementDistibution = [];
    if (value === 'Status') {
      this.engagementDistibutionValue = data.engagementDistributionStatus;
    } else if (value === 'Open Enganement') {
      this.engagementDistibutionValue = data.engagementDistributionOpenEnganement;
    }
    this.defaultEngagementDistributionChart();
  }
  defaultEngagementDistributionChart() {
    const options1 = {
      labels: this.engagementDistibutionValue.labels,
      chart: {
        type: 'donut',
        width: 400,
        height: 400,
      },
      series: this.engagementDistibutionValue.values,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {

          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    this.engagementDistibution = new ApexCharts(
      document.querySelector('#engagementDistibution'),
      options1
    );
    this.engagementDistibution.render();
  }
  enagementTrend() {
    const options = {
      chart: {
        height: 290,
        type: 'area',
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      }, legend: {
        show: false,
      },
      series: [{
        name: 'Activity',
        data: data.enagementTrend.activity
      }, {
        name: 'Task',
        data: data.enagementTrend.task
      }, {
        name: 'Ticket',
        data: data.enagementTrend.ticket
      }],

      xaxis: {
        type: 'Period',
        categories: data.enagementTrend.labels,
      }
    };

    const enagementTrend = new ApexCharts(
      document.querySelector('#enagementTrend'),
      options
    );
    enagementTrend.render();
  }
  fnActivityBreakupChange(value) {
    if (this.activityBreakupChart !== undefined) {
      this.activityBreakupChart.destroy();
    }
    this.activityBreakupChart = [];
    if (value === 'Type') {
      this.activityBreakupValue = data.activityBreakupType;
    } else if (value === 'Purpose') {
      this.activityBreakupValue = data.activityBreakupPurpose;
    }
    this.activityBreakup();
  }
  activityBreakup() {
    const colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
    const options = {
      chart: {
        height: 300,
        type: 'bar',
        events: {
          click(chart, w, e) {
            console.log(chart, w, e);
          }
        },
      },
      colors,
      plotOptions: {
        bar: {
          columnWidth: '45%',
          distributed: true
        }
      },
      dataLabels: {
        enabled: false,
      },
      series: [{
        name: 'Call',
        data: this.activityBreakupValue.call
      }, {
        name: 'Email',
        data: this.activityBreakupValue.email
      },
      {
        name: 'Chat',
        data: this.activityBreakupValue.chat
      }, {
        name: 'Meeting',
        data: this.activityBreakupValue.meeting
      }
      ],
      xaxis: {
        categories: this.activityBreakupValue.periods,
        labels: {
          style: {
            colors,
            fontSize: '14px'
          }
        }
      }
    };

    this.activityBreakupChart = new ApexCharts(
      document.querySelector('#activityBreakupChart'),
      options
    );

    this.activityBreakupChart.render();
  }
  fnTicketChange(value) {
    if (this.ticketChat !== undefined) {
      this.ticketChat.destroy();
    }
    this.ticketChat = [];
    if (value === 'Type') {
      this.fnTicketValue = data.ticketBreakupType;
    } else if (value === 'Status') {
      this.fnTicketValue = data.ticketBreakupStatus;
    } else if (value === 'Assignee') {
      this.fnTicketValue = data.ticketBreakupAssignee;
    } else if (value === 'Priority') {
      this.fnTicketValue = data.ticketBreakupPriority;
    }
    this.ticketChartData();
  }
  ticketChartData() {
    const options1 = {
      labels: this.fnTicketValue.labels,
      chart: {
        type: 'donut',
        height: 305,
      },
      series: this.fnTicketValue.values,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {

          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    this.ticketChat = new ApexCharts(
        document.querySelector('#ticketChat'),
        options1
    );
    this.ticketChat.render();
  }

  viewMoreEngagement(viewContent) {
    this.modalService.open(viewContent, {size: 'lg', centered: true});
  }
}
