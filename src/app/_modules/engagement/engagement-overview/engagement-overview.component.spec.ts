import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngagementOverviewComponent } from './engagement-overview.component';

describe('EngagementOverviewComponent', () => {
  let component: EngagementOverviewComponent;
  let fixture: ComponentFixture<EngagementOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngagementOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngagementOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
