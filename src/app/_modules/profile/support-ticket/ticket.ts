export interface Ticket {
    customerName: string;
    type: string;
    area: string;
    subject: string;
    assignedTo: string;
    priority: string;
    status: string;
    actionDate: string;
    labelbgStatus:string;
    labelbgType:string;

     }
