import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Ticket } from './ticket';

declare var require: any;
const data: any = require('./data.json');

@Component({
  selector: 'app-support-ticket',
  templateUrl: './support-ticket.component.html',
  styleUrls: ['./support-ticket.component.css']
})
export class SupportTicketComponent implements OnInit {
  
  constructor(private modalService: NgbModal) { }

  tickets: Ticket[];
  searchText: any;
  customerName: string;
    type: string;
    area: string;
    subject: string;
    labelbgStatus: string;
    assignedTo: string;
    priority: string;
    status: string;
    actionDate: string;

  //pendingTickets: any;
  //inProgressTickets: any;
  //closedTickets: any;

  open(content) {
    this.modalService.open(content, { size: 'sm', centered: true });
  }

  ngOnInit() {
    this.tickets = data.ticket
  }

  /* pendingTickets = this.tickets.reduce(function (n, ticket) {
    var a:any = (ticket.status == 'Pending');
    return n + a;
  }, 0);
 */

  toggleLabelbg() {
    switch (this.status) {
      case 'Request':
        this.labelbgStatus = 'warning';
        break;

      case 'Complaint':
        this.labelbgStatus = 'primary';
        break;

      case 'Query':
        this.labelbgStatus = 'success';
        break;

      default:
    }
  }

  addTask (){
    this.tickets.push({
    customerName: this.customerName,
    type: this.type,
      area: this.area,
      subject: this.subject,
      assignedTo: this.assignedTo,
      labelbgStatus: this.status == "Closed" ? "warning": "primary",
      priority: this.priority,
      status:this.status,
      actionDate: this.actionDate,
      labelbgType: this.type == "Request" ? "warning": this.type == "Complaint"? "primary" :  "success"
    });
    console.log(this.tickets);
  }
}
