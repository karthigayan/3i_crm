import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';
import { ContactComponent } from './contact/contact.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ChatAppComponent } from './chat-app/chat-app.component';
import { SupportTicketComponent } from './support-ticket/support-ticket.component';
import {ChatService} from './chat-app/chat.service';
import {FormsModule} from '@angular/forms';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {CalendarModule} from 'angular-calendar';
import {QuillModule} from 'ngx-quill';
import {DragulaModule} from 'ng2-dragula';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [ProfileDetailsComponent, ContactComponent, CalendarComponent, ChatAppComponent, SupportTicketComponent],
    imports: [
        CommonModule,
        ProfileRoutingModule,
        FormsModule,
        NgbModule,
        CalendarModule.forRoot(),
        QuillModule,
        DragulaModule,
        Ng2SearchPipeModule
    ],
    providers: [
        ChatService
    ],
})
export class ProfileModule { }
