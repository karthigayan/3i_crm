import { Component, OnInit } from '@angular/core';
import data from './data.json';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css']
})
export class ProfileDetailsComponent implements OnInit {

  customer: any = data;
  constructor() { }

  ngOnInit() {
  }

}
