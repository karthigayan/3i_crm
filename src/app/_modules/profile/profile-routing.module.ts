import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileDetailsComponent} from './profile-details/profile-details.component';
import {CalendarComponent} from './calendar/calendar.component';
import {ChatAppComponent} from './chat-app/chat-app.component';
import {SupportTicketComponent} from './support-ticket/support-ticket.component';

const routes: Routes = [
  {path: '', children: [
      {path: '', component: ProfileDetailsComponent},
      {path: 'calendar', component: CalendarComponent},
      {path: 'chatApp', component: ChatAppComponent },
      {path: 'supportTicket', component: SupportTicketComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
