import { Component, ViewChild, ElementRef, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ChatService } from './chat.service';
import { Chat } from './chat';

@Component({
  selector: 'app-chat-app',
  templateUrl: './chat-app.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./chat-app.component.css'],
})
export class ChatAppComponent implements OnInit {


  public now: Date = new Date();
  public showSidebar = false;

  chat: Chat[];
  activeChatUser: string;
  activeChatUserImg: string;
  activeChatUserStatus: string;
  @ViewChild('messageInput', { static: true }) messageInputRef: ElementRef;

  messages = new Array();
  item = 0;
  constructor(private elRef: ElementRef, private chatService: ChatService) {
    this.chat = chatService.chat1;
    this.activeChatUser = 'Steve Rogers';
    this.activeChatUserImg = 'assets/images/users/1.jpg';
    this.activeChatUserStatus = 'Online';

    //  For Get Current Time
    setInterval(() => {
      this.now = new Date();
    }, 1);
  }

  ngOnInit() {}

  // send button function calls
  onAddMessage() {
    if (this.messageInputRef.nativeElement.value != "") {
      this.messages.push(this.messageInputRef.nativeElement.value);
    }
    this.messageInputRef.nativeElement.value = "";
    this.messageInputRef.nativeElement.focus();
  }


  // chat user list click event function
  SetActive(event, chatId: string) {
    var hElement: HTMLElement = this.elRef.nativeElement;
    // now you can simply get your elements with their class name
    var allAnchors = hElement.getElementsByClassName('message-item');
    // do something with selected elements
    [].forEach.call(allAnchors, function (item: HTMLElement) {
      item.setAttribute('class', 'message-item');
    });
    // set active class for selected item
    event.currentTarget.setAttribute('class', 'message-item');

    this.messages = [];

    if (chatId === 'chat1') {
      this.chat = this.chatService.chat1;
      this.activeChatUser = 'Steve Rogers';
      this.activeChatUserImg = 'assets/images/users/1.jpg';
      this.activeChatUserStatus = 'Online';
    } else if (chatId === 'chat2') {
      this.chat = this.chatService.chat2;
      this.activeChatUser = 'Tony  Stark';
      this.activeChatUserImg = 'assets/images/users/2.jpg';
      this.activeChatUserStatus = 'Busy';
    } else if (chatId === 'chat3') {
      this.chat = this.chatService.chat3;
      this.activeChatUser = 'Buckey Barnes';
      this.activeChatUserImg = 'assets/images/users/3.jpg';
      this.activeChatUserStatus = 'Away';
    } else if (chatId === 'chat4') {
      this.chat = this.chatService.chat4;
      this.activeChatUser = 'Natasha Romanoff';
      this.activeChatUserImg = 'assets/images/users/4.jpg';
      this.activeChatUserStatus = 'Away';
    } else if (chatId === 'chat5') {
      this.chat = this.chatService.chat5;
      this.activeChatUser = 'Maria Hill';
      this.activeChatUserImg = 'assets/images/users/5.jpg';
      this.activeChatUserStatus = 'Online';
    } else if (chatId === 'chat6') {
      this.chat = this.chatService.chat6;
      this.activeChatUser = 'Wanda Maximoff';
      this.activeChatUserImg = 'assets/images/users/6.jpg';
      this.activeChatUserStatus = 'Online';
    } else if (chatId === 'chat7') {
      this.chat = this.chatService.chat7;
      this.activeChatUser = 'Carol Danevars';
      this.activeChatUserImg = 'assets/images/users/7.jpg';
      this.activeChatUserStatus = 'Away';
    }

    if (chatId === 'chat1') {
      this.chat = this.chatService.chat1;
    } else if (chatId === 'chat2') {
      this.chat = this.chatService.chat2;
    } else if (chatId === 'chat3') {
      this.chat = this.chatService.chat3;
    } else if (chatId === 'chat4') {
      this.chat = this.chatService.chat4;
    } else if (chatId === 'chat5') {
      this.chat = this.chatService.chat5;
    } else if (chatId === 'chat6') {
      this.chat = this.chatService.chat6;
    } else if (chatId === 'chat7') {
      this.chat = this.chatService.chat7;
    }

  }

  mobileSidebar() {
    this.showSidebar = !this.showSidebar;
  }

}
