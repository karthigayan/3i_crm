import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapAppComponent } from './chat-app.component';

describe('ChapAppComponent', () => {
  let component: ChapAppComponent;
  let fixture: ComponentFixture<ChapAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChapAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
