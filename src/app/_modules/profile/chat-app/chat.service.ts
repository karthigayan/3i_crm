import { Injectable } from '@angular/core';
import { Chat } from './chat';

@Injectable()
export class ChatService {

    constructor() { }

    public chat1: Chat[] = [
    ];
    public chat2: Chat[] = [
        /*new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.32 am',
            [
                'Hi'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/2.jpg',
            '9.32 am',
            [
                'Hi, Bruce'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.32 am',
            [
                'Have a knowledge about Cordova ?'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/2.jpg',
            '9.34 am',
            [
                'Yes, You also have interest in that ?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.34 am',
            [
                'Yeah, I have also develop a mobile app on that'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/2.jpg',
            '9.34 am',
            [
                'Do you upload that on play store ?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.34 am',
            [
                'Nope'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/2.jpg',
            '9.35 am',
            [
                'Then Upload it, you will get money.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.35 am',
            [
                'Okay.'
            ],
        ),*/
    ];
    public chat3: Chat[] = [
        /*new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.37 am',
            [
                'Hey, How are you ?'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/3.jpg',
            '9.37 am',
            [
                'Hi, I am fine.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.37 am',
            [
                'okay'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/3.jpg',
            '9.37 am',
            [
                'Lets go for tea.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.39 am',
            [
                'Yeah, Sure'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/3.jpg',
            '9.39 am',
            [
                'What about time?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.39 am',
            [
                'I will ask you after'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/3.jpg',
            '9.39 am',
            [
                'Okay, let me know. Once you decide.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.41 am',
            [
                'Yeah'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/3.jpg',
            '9.41 am',
            [
                'Bye'
            ],
        ),*/
    ];
    public chat4: Chat[] = [
        /*new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.21 am',
            [
                'Hi, How are you ?'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/4.jpg',
            '9.21 am',
            [
                'Hey Bruce, I am Fine'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.21 am',
            [
                'What are you doing nowadays ?'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/4.jpg',
            '9.21 am',
            [
                'Nothing Much.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.21 am',
            [
                'yes'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/4.jpg',
            '9.22 am',
            [
                'Do you have interest in IOS ?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.23 am',
            [
                'Yes, not more.'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/4.jpg',
            '9.24 am',
            [
                'Okay.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.24 am',
            [
                'I have join Android Classes.'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/4.jpg',
            '9.24 am',
            [
                'Okay, Great.'
            ],
        ),*/
    ];
    public chat5: Chat[] = [
        /*new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.14 am',
            [
                'Hi'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/5.jpg',
            '9.14 am',
            [
                'Hi, Bruce'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.14 am',
            [
                'Have a knowledge about Cordova ?'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/5.jpg',
            '9.15 am',
            [
                'Yes, You also have interest in that ?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.15 am',
            [
                'Yeah, I have also develop a mobile app on that'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/5.jpg',
            '9.15 am',
            [
                'Do you upload that on play store ?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.16 am',
            [
                'Nope'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/5.jpg',
            '9.16 am',
            [
                'Then Upload it, you will get money.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.16 am',
            [
                'Okay.'
            ],
        ),*/
    ];
    public chat6: Chat[] = [
       /* new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.16 am',
            [
                'Hey, How are you ?'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/6.jpg',
            '9.18 am',
            [
                'Hi, I am fine.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.18 am',
            [
                'okay'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/6.jpg',
            '9.18 am',
            [
                'Lets go for tea.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.18 am',
            [
                'Yeah, Sure'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/6.jpg',
            '9.18 am',
            [
                'What about time?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.18 am',
            [
                'I will ask you after'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/6.jpg',
            '9.19 am',
            [
                'Okay, let me know. Once you decide.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.19 am',
            [
                'Yeah'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/6.jpg',
            '9.19 am',
            [
                'Bye'
            ],
        ),*/
    ];
    public chat7: Chat[] = [
        /*new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.00 am',
            [
                'Hi, How are you ?'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/7.jpg',
            '9.00 am',
            [
                'Hey Bruce, I am Fine'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.01 am',
            [
                'What are you doing nowadays ?',
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/7.jpg',
            '9.01 am',
            [
                'Nothing Much.'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.01 am',
            [
                'yes'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/7.jpg',
            '9.01 am',
            [
                'Do you have interest in IOS ?'
            ],
        ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.01 am',
            [
                'Yes, not more.'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/7.jpg',
            '9.03 am',
            [
                'Okay.'
            ],
            ),
        new Chat(
            'odd',
            'assets/images/users/8.jpg',
            '9.03 am',
            [
                'I have join Android Classes.'
            ],
        ),
        new Chat(
            'even',
            'assets/images/users/7.jpg',
            '9.03 am',
            [
                'Okay, Great.'
            ],
        ),*/
    ];
}
