import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import {MaterialModuleModule} from '../../_custom_components/material-module/material-module.module';
import {Ng5SliderModule} from 'ng5-slider';
import { CustomerOverviewComponent } from './customer-overview/customer-overview.component';
import {SparklineModule} from '@progress/kendo-angular-charts';
import {ChartsModule} from 'ng2-charts';
import {ChartistModule} from 'ng-chartist';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [CustomerListComponent, CustomerOverviewComponent],
    imports: [
        CommonModule,
        CustomerRoutingModule,
        MaterialModuleModule,
        Ng5SliderModule,
        SparklineModule,
        ChartsModule,
        ChartistModule,
        FormsModule,
        NgbModule
    ]
})
export class CustomerModule { }
