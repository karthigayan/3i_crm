import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CustomerListComponent} from './customer-list/customer-list.component';
import {CustomerOverviewComponent} from './customer-overview/customer-overview.component';

const routes: Routes = [
  {path: '' , children: [
      {path: '', component: CustomerListComponent },
      {path: 'customer-overview', component: CustomerOverviewComponent}
    ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
