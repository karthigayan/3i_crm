import { Component, OnInit } from '@angular/core';
import data from './data.json';
import {Router} from '@angular/router';
import {LabelType, Options} from 'ng5-slider';
// @ts-ignore
import ApexCharts from 'apexcharts';
import {CustomerDetailService} from '../../../_service/customer/customer-detail.service';
import {currency} from '../../../_config/currency';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  panelOpenState = false;
  customer: any = data;
  public result: any;
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  // ------------ Range Slider ----------
  // one
  value = 0;
  highValue = 18000000;
  options: Options = {
    floor: 0,
    ceil: 50000000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        case LabelType.High:
          return  '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        default:
          return '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      }
    }
  };

  // two
  value2 = 0;
  highValue2 = 40000000;
  options2: Options = {
    floor: 0,
    ceil: 60000000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        case LabelType.High:
          return '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        default:
          return '$ ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      }
    }
  };

  // three
  value3 = 0;
  highValue3 = 80;
  options3: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + '%';
        case LabelType.High:
          return  value + '%';
        default:
          return value + '%';
      }
    }
  };

  // four
  value4 = 0;
  highValue4 = 50;
  options4: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + '%';
        case LabelType.High:
          return  value + '%';
        default:
          return value + '%';
      }
    }
  };

  // five
  value5 = 0;
  highValue5 = 60;
  options5: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + '%';
        case LabelType.High:
          return  value + '%';
        default:
          return value + '%';
      }
    }
  };
  // --------------- Filter ------------------
  filter = { aggressive: false, moderate: false, riskAverse: false, conservative: false, calamba: false, makati: false };
  constructor(private router: Router,
              private cutId: CustomerDetailService) {}

  ngOnInit() {
    if (localStorage.getItem('customer') === undefined || localStorage.getItem('customer') === null) {
      for (let id = 0; id < this.customer.length; id++) {
        const obj = this.customer[id];
        obj.flag = false;
      }
      localStorage.setItem('customer', JSON.stringify(this.customer));
    } else {
      this.customer = JSON.parse(localStorage.getItem('customer'));
    }

    this.result = this.customer;
    // const filterValue = 35;
    // const result = data.filter(d => d.Age >  35 && d.Age <  40);
    // console.log(data.flat(1));
    // const result1 = data.flat(1);
   // this.Custfilter();
    // --------------------  SparkLine Bar Chart Start ----------------------------
    const options8 = {
      chart: {
        type: 'bar',
        width: 120,
        height: 60,
        sparkline: {
          enabled: true
        }
      },
      fill: {
        color: ['#f00']
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const options9 = {
      chart: {
        type: 'bar',
        width: 120,
        height: 60,
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };

    new ApexCharts(document.querySelector('#sparkChart'), options8).render();
    new ApexCharts(document.querySelector('#sparkChart1'), options9).render();
    new ApexCharts(document.querySelector('#sparkChart2'), options8).render();
    new ApexCharts(document.querySelector('#sparkChart3'), options8).render();
    // -------------------- SparkLine Bar Chart End ----------------------------------

    // -------------------- SparkLine Area Chart Start ------------------------------
    const spark3 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 35,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0.3
      },
      series: [{
        data: [47, 45, 54, 38, 56, 24, 65]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        min: 0
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };

    new ApexCharts(document.querySelector('#sparkAreaChart'), spark3).render();
    new ApexCharts(document.querySelector('#sparkAreaChart1'), spark3).render();
    // --------------------- SparkLine Area Chart End ----------------------------------
  }
  onNavigate(id) {
    // tslint:disable-next-line:radix
     this.cutId.setMyCustomer(parseInt(id));
     this.router.navigate(['/overview/customer']);
  }
  changeFlag(item) {
    item.flag = true;
    localStorage.setItem('customer', JSON.stringify(this.customer));
  }
  Custfilter() {

    // -----------------------  Range Slider Filter -----------------------------
    // tslint:disable-next-line:radix
    this.result = data.filter(d => (parseInt(d.Asset_Under_Management) >  this.value && parseInt(d.Asset_Under_Management) <  this.highValue) &&
        // tslint:disable-next-line:radix
        (parseInt(d.Investible_Cash) >  this.value2 && parseInt(d.Investible_Cash) <  this.highValue2)
        && (parseFloat(d.Equity_Allocation) >  (this.value3 / 100) && parseFloat(d.Equity_Allocation) <  (this.highValue3 / 100))
        && (parseFloat(d.Fixed_Income_Allocation) >  (this.value4 / 100) && parseFloat(d.Fixed_Income_Allocation) <  (this.highValue4 / 100))
        && (parseFloat(d.Alternate_Alloation) >  (this.value5 / 100) && parseFloat(d.Alternate_Alloation) <  (this.highValue5 / 100)));
    console.log(this.result.length);
    // ----------------------- Risk Profile Filter ---------------------------
    if (this.filter.aggressive || this.filter.conservative || this.filter.moderate || this.filter.riskAverse ) {
        this.result = this.result.filter(d => (d.Risk_Profile === 'Aggressive' && this.filter.aggressive)
            || (d.Risk_Profile === 'Conservative' && this.filter.conservative)
            || (d.Risk_Profile === 'Moderate' && this.filter.moderate)
            || (d.Risk_Profile === 'Risk averse' && this.filter.riskAverse)
        );
    }
    // ----------------------- Location Filter ---------------------------
    if (this.filter.calamba || this.filter.makati) {
      this.result = this.result.filter(d => (d.Location === 'Albany' && this.filter.calamba)
          || (d.Location === 'Brooklyn' && this.filter.makati));
    }
    console.log(this.result);
    // tslint:disable-next-line:radix
    this.result = this.result.sort((a, b) => parseInt(a.Customer_ID) - parseInt(b.Customer_ID ));
  }
}
