import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist/dist/chartist.component';
// @ts-ignore
import ApexCharts from 'apexcharts';
import bubbleChart from './data/bubblechart.json';
import analysis from './data/analysis.json';
import happy from './data/happy.json';
import unhappy from './data/unhappy.json';
import neutral from './data/neutral.json';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {currency} from '../../../_config/currency';

declare var require: any;

const data: any = require('./data/data.json');

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}
@Component({
  selector: 'app-customer-overview',
  templateUrl: './customer-overview.component.html',
  styleUrls: ['./customer-overview.component.css']
})
export class CustomerOverviewComponent implements OnInit {
  public topBand2 = data.customer_band2;
  public riskChart: any;
  public riskValue: any;
  public riskLabel: any;
  public attributeValue: any;
  public demographicValue: any;
  public demographicLabel: any;
  public demographicChart: any;
  public bubbleData = [];
  public bubblechart: any;
  public bubbleOptions: any;
  public analysisTable: any;
  public analysisData = analysis;
/*  public bubbleValue = bubbleChart.equity;*/
  public bubbleValue: any;
  public happyValue = happy;
  public unhappyValue = unhappy;
  public neutralValue = neutral;
  public modalTitle = '';
  // public bgColor: any;
  public i = 0;
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  positiveStyles = {
    width: this.analysisData[0].percentage + '%',
    height: '6px'
  };
  negativeStyles = {
    width: this.analysisData[1].percentage + '%',
    height: '6px'
  };
  neutralStyles = {
    width: this.analysisData[2].percentage + '%',
    height: '6px'
  };
  // -----Spark Line -----------------
  public data1: any[] = [
    936, 968, 1025, 999, 998, 1014, 1017, 1010, 1010, 1007
  ];

  public seriesData: number[] = [20, 40, 45, 30, 50];
  public categories: string[] = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
  // Doughnut1
  public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData: number[] = [350, 450, 100];
  public doughnutChartType = 'doughnut';
  public chartColors: Array<any> = [
    {
      backgroundColor: ['rgba(132,197,241,1)', '#fd9fb6', '#fde098', '#A8B3C5', '#616774'],
      hoverBackgroundColor: ['#03a9f4', '#fe6e8c', '#ff9800', '#949FB1', '#4D5360'],
      borderWidth: 0,
    }
  ];
// Scatter chart
  scatterChart1: Chart = {
    type: 'Line',
    data: data.Scatter,
    options: {
      showLine: false,
      height: 400,
      axisX: {
        labelInterpolationFnc(value: number, index: number): string {
          return index % 13 === 0 ? `W${value}` : null;
        }
      }
    },
    responsiveOptions: [
      [
        'screen and (min-width: 640px)',
        {
          axisX: {
            labelInterpolationFnc(value: number, index: number): string {
              return index % 4 === 0 ? `W${value}` : null;
            }
          }
        }
      ]
    ]
  };
  // lineChart
  public lineChartData1: Array<any> = [{ data: [1, 1, 1, 11, 1, 1, 11, 1, 1, 13,
      1, 3, 1, 1, 1, 3, 2, 1, 1, 1, 1, 1, 1,
      8, 1, 1, 1, 1, 12, 1, 1, 1, 1, 1], label: 'Sales' }];

  public lineChartLabels1: Array<any> = [0.02, 0.03, 0.04, 0.07, 0.08, 0.09,
    0.1, 0.11, 0.12, 0.15, 0.17, 0.2, 0.21, 0.22, 0.25, 0.28, 0.3, 0.34, 0.39, 0.67, 0.75,
    1.07, 1.35, 1.5, 1.7, 1.8, 1.83, 1.85, 2, 2.67, 5, 5.56, 5.78, 7.78];
  public lineChartOptions1: any = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          },
          gridLines: {
            color: 'rgba(0, 0, 0, 0.1)'
          }
        }
      ],
      xAxes: [
        {
          gridLines: {
            color: 'rgba(0, 0, 0, 0.1)'
          }
        }
      ]
    },
    lineTension: 5,
    responsive: true,
    maintainAspectRatio: false,
    elements: { line: { tension: 0 } }
  };

  public userData: any;
  public allUserData: any;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.demographicChange('1');
    this.gradientChart();
    this.riskChange('1');
    // tslint:disable-next-line:no-shadowed-variable
    this.userData = data.revenue.filter((item, index) => index => 0 );
    this.allUserData = data.revenue;
    this.bubbleSelect('equity');
    // --------------------  SparkLine Bar Chart Start ----------------------------
    const customerManaged = {
      chart: {
        type: 'bar',
        width: 150,
        height: 80,
        sparkline: {
          enabled: true
        }
      },
      fill: {
        colors: ['#0d880d'],
        opacity: 0.7
      },
      plotOptions: {
        bar: {
          columnWidth: '50%'
        }
      },
      series: [{
        data: [85, 80, 85, 82, 88, 85]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const customerLost = {
      chart: {
        type: 'bar',
        width: 150,
        height: 80,
        sparkline: {
          enabled: true
        }
      },
      fill: {
        colors: ['#f00'],
        opacity: 0.6
      },
      plotOptions: {
        bar: {
          columnWidth: '50%'
        }
      },
      series: [{
        data: [12, 10, 10, 6, 9, 2]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const lowOption = {
      chart: {
        type: 'bar',
        width: 100,
        height: 50,
        sparkline: {
          enabled: true
        }
      },
      fill: {
        colors: ['#36f029'],
        opacity: 0.6
      },
      plotOptions: {
        bar: {
          columnWidth: '50%'
        }
      },
      series: [{
        data: [44, 40, 32, 51, 58]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
          }
        },
        marker: {
          show: false
        }
      }
    };
    const highOption = {
      chart: {
        type: 'bar',
        width: 100,
        height: 50,
        sparkline: {
          enabled: true
        }
      },
      fill: {
        colors: ['#f02929'],
        opacity: 0.6
      },
      plotOptions: {
        bar: {
          columnWidth: '50%'
        }
      },
      series: [{
        data: [45, 49, 57, 38, 31]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {

          }
        },
        marker: {
          show: false
        }
      }
    };

    new ApexCharts(document.querySelector('#sparkChart'), customerManaged).render();
    new ApexCharts(document.querySelector('#sparkChart1'), customerLost).render();
    new ApexCharts(document.querySelector('#sparkChart2'), lowOption).render();
    new ApexCharts(document.querySelector('#sparkChart3'), highOption).render();
    // -------------------- SparkLine Bar Chart End ----------------------------------

    // -------------------- SparkLine Area Chart Start ------------------------------
    const areaChart1 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 59,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0.3
      },
      series: [{
        data: [199467960, 203457319, 216813000, 255074118, 289856952, 258800850, 213885000, 220500000]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        labels: {
          formatter(value) {
            return (value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const areaChart2 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 59,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0.3
      },
      series: [{
        data: [157772020, 159365677, 156240860, 169827021, 197473280, 199467960, 255074118, 213885000]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        labels: {
          formatter(value) {
            return (value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };

    new ApexCharts(document.querySelector('#sparkAreaChart'), areaChart1).render();
    new ApexCharts(document.querySelector('#sparkAreaChart1'), areaChart2).render();
    // --------------------- SparkLine Area Chart End ----------------------------------
    const options = {
      chart: {
        height: 280,
        type: 'bar',
        stacked: true,
      },
      plotOptions: {
        bar: {
          horizontal: false,
        },

      },
      stroke: {
        width: 1,
        colors: ['#fff']
      },
      series: [{
        name: 'Managed',
        data: [85, 80, 85, 82, 88, 85]
      }, {
        name: 'Aqquired',
        data: [7, 15, 7, 12, 6, 6]
      }, {
        name: 'Lost',
        data: [-12, -10, -10, -6, -9, -2]
      }],
      xaxis: {
        categories: ['Oct-18', 'Jan-19', 'Apr-19', 'Jul-19', 'Oct-19', 'Current'
        ],
        labels: {
          formatter(val) {
            return val;
          }
        }
      },
      yaxis: {
        title: {
          text: undefined
        },

      },
      tooltip: {
        y: {
          formatter(val) {
            return val;
          }
        }
      },
      fill: {
        opacity: 1

      },

      legend: {
        position: 'top',
        horizontalAlign: 'left',
        offsetX: 40
      }
    };

    const chart = new ApexCharts(
        document.querySelector('#stackedChart'),
        options
    );

    chart.render();

    // ---------------------- Bubble Chart -------------------------
  }
  // -------------  Bubble Function -------------
  dynamicBubble() {
    this.bubbleData = [];
    this.bubbleValue.forEach(obj => {
      this.bubbleData.push({
        name: 'Client: ' + obj.Client_Name + ', Risk Score',
        data: [
          // tslint:disable-next-line:radix
            [parseInt(obj.Risk_Score ), parseInt(obj.Allocation ), parseInt(obj.Asset_Under_Management)]
        ]
      });
    });
    this.bubbleOptions = {
      chart: {
        height: 320,
        type: 'bubble',
      },
      dataLabels: {
        enabled: false
      },
      legend: {
        show: false
      },
      series: this.bubbleData,
      fill: {
        opacity: 0.8,
      },
      title: {
        text: ''
      },
      xaxis: {
        min: 0,
        max: 100
      },
      yaxis: {
        min: 0,
        max: 80
      }
    };

    this.bubblechart = new ApexCharts(
        document.querySelector('#Bubblechart'),
        this.bubbleOptions
    );
    this.bubblechart.render();
  }
  // ------------------ Model Options -------------------
  openLg(content3, item) {
    if (item === 'positive') {
      this.analysisTable = this.happyValue;
      this.modalTitle = 'Positive';

      // this.bgColor = {
      //   background:'green'
      // },
      // {
      //   background:'orange'
      // },
      // {
      //   background:'red'
      // }


    } else if (item === 'negative') {
      this.analysisTable = this.unhappyValue;
      this.modalTitle = 'Negative';
    } else {
      this.analysisTable = this.neutralValue;
      this.modalTitle = 'Neutral';
    }
    this.modalService.open(content3, {
      size: 'lg',
      centered: true});

  }
  // ------------------------ Bubble Chart Dropdown --------------------
  bubbleSelect(newValue) {
    if (this.bubblechart !== undefined) {
      this.bubblechart.destroy();
    }
    this.bubbleValue = [];
    if (newValue === 'equity') {
      this.bubbleValue = bubbleChart.equity;
    } else if (newValue === 'fixedIncome') {
      this.bubbleValue = bubbleChart.fixedIncome;
    } else if (newValue === 'alternate') {
      this.bubbleValue = bubbleChart.alternate;
    } else if (newValue === 'cash') {
      this.bubbleValue = bubbleChart.cash;
    }
    this.dynamicBubble();
  }
  fnUserCardChange(value) {
    if (value === 'Revenue') {
      // tslint:disable-next-line:no-shadowed-variable
      this.userData = data.revenue.filter((item, index) => index => 0 );
      this.allUserData = data.revenue;
    } else if (value === 'AUM') {
      // tslint:disable-next-line:no-shadowed-variable
      this.userData = data.AUM.filter((item, index) => index => 0 );
      this.allUserData = data.AUM;
    }
  }
  // ------------------- key Attribute modal ---------------------
  keyAttribute(content4, item) {
    this.attributeValue = [];
    if (item === 'outperformer') {
      this.attributeValue = data.attribute;
      this.modalTitle = 'Outperformer';
    } else if (item === 'underperformer') {
      this.attributeValue = data.underperformer;
      this.modalTitle = 'Underperformer';
    } else if (item === 'dormant') {
      this.attributeValue = data.dormant;
      this.modalTitle = 'Dormant';
    } else if (item === 'larger_withdrawl') {
      this.attributeValue = data.larger_withdrawl;
      this.modalTitle = 'Larger Withdrawl';
    } else if (item === 'recent_contribution') {
      this.attributeValue = data.underperformer;
      this.modalTitle = 'Recent Contribution';
    }
    this.modalService.open(content4, {
      size: 'lg',
      centered: true});
  }

  // --------------------- Donut Chart Start --------------------
  demoGraphicChart() {
    const demooptions = {
      plotOptions: {
        pie: {
          donut: {
            size: '50%'
          }
        }
      },
      labels: this.demographicLabel,
      dataLabels: {
        enabled: false
      },
      chart: {
        type: 'donut',
        height: 300
      },
      series: this.demographicValue,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {

          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    this.demographicChart = new ApexCharts(
        document.querySelector('#demograpchart'),
        demooptions
    );

    this.demographicChart.render();
  }
  demographicChange(event) {
    if (this.demographicChart !== undefined) {
      this.demographicChart.destroy();
    }
    this.demographicValue = [];
    this.demographicLabel = [];
    if (event === '1') {
      data.demographic.occupation.forEach(obj => {
        this.demographicValue.push(
            // tslint:disable-next-line:radix
            parseInt(obj.value)
        );
        this.demographicLabel.push(
            obj.Row_Labels
        );
      });
    } else if (event === '2') {
      data.demographic.location.forEach(obj => {
        this.demographicValue.push(
            // tslint:disable-next-line:radix
            parseInt(obj.value)
        );
        this.demographicLabel.push(
            obj.Row_Labels
        );
      });
    } else if (event === '3') {
      data.demographic.networth.forEach(obj => {
        this.demographicValue.push(
            // tslint:disable-next-line:radix
            parseInt(obj.value)
        );
        this.demographicLabel.push(
            obj.Row_Labels
        );
      });
    } else if (event === '4') {
      data.demographic.income.forEach(obj => {
        this.demographicValue.push(
            // tslint:disable-next-line:radix
            parseInt(obj.value)
        );
        this.demographicLabel.push(
            obj.Row_Labels
        );
      });
    } else if (event === '5') {
      data.demographic.category.forEach(obj => {
        this.demographicValue.push(
            // tslint:disable-next-line:radix
            parseInt(obj.value)
        );
        this.demographicLabel.push(
            obj.Row_Labels
        );
      });
    }
    this.demoGraphicChart();
  }
  // --------------------- Donut Chart End ----------------------

  // --------------------- Gradient Line Chart--------------------
  gradientChart() {
    const options = {
      chart: {
        height: 228,
        type: 'bar',
      },
      plotOptions: {
        bar: {
          horizontal: false,
        }
      },
      dataLabels: {
        enabled: false
      },
      series: [{
        data: [56, 5, 16, 10, 2]
      }],
      xaxis: {
        categories: ['Below 0.5 X', 'Above 2 X', '1.5 X - 2 X', '1.0 - 1.5 X', '0.5 - 1.0 X'],
      }
    };

    const chart = new ApexCharts(
        document.querySelector('#gradientChart'),
        options
    );

    chart.render();
  }
  // ----------------- Pie Chart --------------------------
  // Pie Chart
  riskProfile() {
    const riskOptions = {
      chart: {
        width: 400,
        height:300,
        type: 'pie',
      },
      labels: this.riskLabel,
      series: this.riskValue,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    this.riskChart = new ApexCharts(
        document.querySelector('#riskChart'),
        riskOptions
    );

    this.riskChart.render();
  }
  riskChange(event) {
    if (this.riskChart !== undefined) {
      this.riskChart.destroy();
    }
    this.riskValue = [];
    this.riskLabel = [];
    if (event === '1') {
      data.riskChart1.forEach(obj => {
        this.riskValue.push(
            // tslint:disable-next-line:radix
            parseInt(obj.value)
        );
        this.riskLabel.push(
            obj.Row_Labels
        );
      });
    } else if (event === '2') {
      data.riskChart2.forEach(obj => {
        this.riskValue.push(
            // tslint:disable-next-line:radix
            parseInt(obj.value)
        );
        this.riskLabel.push(
            obj.Row_Labels
        );
      });
    }
    this.riskProfile();
  }
  // -------------------  modal for customer ------------
  openUser(content5) {
    this.modalService.open(content5, {
      size: 'lg',
      centered: true});
  }
}
