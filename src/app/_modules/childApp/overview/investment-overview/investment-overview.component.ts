import { Component, OnInit } from '@angular/core';
import customer from './customer.json';
import chartValue from './chart.json';
import ApexCharts from 'apexcharts';
import {ActivatedRoute} from '@angular/router';
import {currency} from '../../../../_config/currency';
import {CustomerDetailService} from '../../../../_service/customer/customer-detail.service';

@Component({
  selector: 'app-investment-overview',
  templateUrl: './investment-overview.component.html',
  styleUrls: ['./investment-overview.component.css']
})
export class InvestmentOverviewComponent implements OnInit {
  currentRate = 8;
  public customerId: any;
  public selectedCustomer: any;
  public CUR = currency.BASE_CUR;
  // ----------------------
  public areaStackChart1: any;
  public areaStackValueIv1: any;
  public areaStackValueCv1: any;
  public areaStackValueBv1: any;
  public areaStackLabel1: any;
  public performValue: any;
  public portfolioValue: any;
  // -----------
  public opportunityValue: any;
  public activityValue: any;
  public ticketValue: any;
  public opportunityData: any;
  public activityData: any;
  public ticketData: any;
  public heldData: any;
  public activitySchedule: any;
  public pulse: any;
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  constructor(private router: ActivatedRoute,
              private custId: CustomerDetailService) {
    // this.customerId = this.router.snapshot.paramMap.get('id');
    this.customerId = custId.getMyCustomer();
    this.selectedCustomer = customer.filter( val => parseInt(val.Customer_ID, 10) === this.customerId);
    console.log(this.selectedCustomer);
    this.performValue = chartValue.portfolio_value.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.opportunityValue = chartValue.opportunity.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.activityValue = chartValue.activity.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.ticketValue = chartValue.ticket.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.opportunityData = chartValue.opportunityItem.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.activityData = chartValue.activityItem.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.ticketData = chartValue.ticketItem.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.heldData = chartValue.held.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.activitySchedule = chartValue.activity_Schedule.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.pulse = chartValue.pulseValue.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
  }
  ngOnInit() {
    this.areaPortfolioChange2('monthly');
    const spark3 = {
      chart: {
        type: 'area',
        hwidth: 100,
        height: 95,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        opacity: 0.3
      },
      series: [{
        data: [47, 45, 54, 38, 56, 24, 65]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        min: 0
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    new ApexCharts(document.querySelector('#sparkAreaChart'), spark3).render();
  }
  areaPortfolioChart2() {
    const options = {
      chart: {
        height: 280,
        type: 'area',
        stacked: true,
        events: {
          selection(chart, e) {
          }
        },

      },
      colors: ['#008FFB', '#00E396', '#CED4DC'],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },

      series: [{
        name: 'Total Revenue',
        data: this.areaStackValueIv1
      },
        {
          name: 'Fee Charges',
          data: this.areaStackValueCv1
        },
        {
          name: 'Commission',
          data: this.areaStackValueBv1
        }
      ],
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 0.6,
          opacityTo: 0.8,
        }
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left'
      },
      xaxis: {
        categories: this.areaStackLabel1
      },
    };
    this.areaStackChart1 = new ApexCharts(document.querySelector('#areaStackChart1'), options);
    this.areaStackChart1.render();
  }
  areaPortfolioChange2(event) {
    if (this.areaStackChart1 !== undefined) {
      this.areaStackChart1.destroy();
    }
    this.areaStackValueIv1 = [];
    this.areaStackValueCv1 = [];
    this.areaStackValueBv1 = [];
    this.areaStackLabel1 = [];
    if (event === 'monthly') {
      this.portfolioValue = this.performValue.filter( val => val.Period === 'Monthly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv1.push(parseInt(obj.Total_Revenue, 10));
        this.areaStackValueCv1.push(parseInt(obj.Fee_Charges, 10));
        this.areaStackValueBv1.push(parseInt(obj.Commission, 10));
        this.areaStackLabel1.push(obj.Data_Value);
      });

    } else if (event === 'quarterly') {
      this.portfolioValue = this.performValue.filter( val => val.Period === 'Quarterly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv1.push(parseInt(obj.Total_Revenue, 10));
        this.areaStackValueCv1.push(parseInt(obj.Fee_Charges, 10));
        this.areaStackValueBv1.push(parseInt(obj.Commission, 10));
        this.areaStackLabel1.push(obj.Data_Value);
      });
    } else if (event === 'yearly') {
      this.portfolioValue = this.performValue.filter( val => val.Period === 'Yearly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv1.push(parseInt(obj.Total_Revenue, 10));
        this.areaStackValueCv1.push(parseInt(obj.Fee_Charges, 10));
        this.areaStackValueBv1.push(parseInt(obj.Commission, 10));
        this.areaStackLabel1.push(obj.Data_Value);
      });
    }
    this.areaPortfolioChart2();
  }
}
