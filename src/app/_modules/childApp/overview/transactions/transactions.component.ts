import { Component, OnInit } from '@angular/core';
// @ts-ignore
import ApexCharts from 'apexcharts';
import {CustomerDetailService} from '../../../../_service/customer/customer-detail.service';
import data from './data.json';
import {currency} from 'src/app/_config/currency';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
    public customerId: any;
    public transaction: any;
    public accordValue: any;
    currency = currency.BASE_CUR;
    cSymbol = currency.BASE_CUR_SYL;

    constructor(private custId: CustomerDetailService) {
        console.log(data.trans);
        this.customerId = custId.getMyCustomer();
        this.transaction = data.trans.filter( val => parseInt(val.Client_ID, 10) === this.customerId);
        console.log(this.transaction);
    }

    ngOnInit() {
        const options = {
            chart: {
                height: 538,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            dataLabels: {
                enabled: true
            },
            series: [{
                data: [50, -20, 60, 20, 30, -10]
            }],
            xaxis: {
                categories: ['Equity Mutual Fund', 'Direct Equity', 'Debt Mutual Fund', 'Cash Mutual Fund',
                    'Alternates Mutual Fund', 'Alternates'],
            }
        };

        const chart = new ApexCharts(
            document.querySelector('#chart'),
            options
        );


        chart.render();

        const options8 = {
            chart: {
                type: 'bar',
                width: 100,
                height: 35,
                sparkline: {
                    enabled: true
                }
            },
            plotOptions: {
                bar: {
                    columnWidth: '80%'
                }
            },
            series: [{
                data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
            }],
            labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
            xaxis: {
                crosshairs: {
                    width: 1
                },
            },
            tooltip: {
                fixed: {
                    enabled: false
                },
                x: {
                    show: false
                },
                y: {
                    title: {
                        formatter(seriesName) {
                            return '';
                        }
                    }
                },
                marker: {
                    show: false
                }
            }
        };

        new ApexCharts(document.querySelector('#sparkChart'), options8).render();
    }
    transValue(value) {
        this.accordValue = [];
        this.accordValue = data.orderValue.filter( val => parseInt(val.Order_ID, 10) === parseInt(value, 10));
    }

}
