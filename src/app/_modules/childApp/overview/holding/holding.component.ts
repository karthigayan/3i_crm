import { Component, OnInit } from '@angular/core';
import data from './data.json';
import {CustomerDetailService} from '../../../../_service/customer/customer-detail.service';
import {currency} from 'src/app/_config/currency';

@Component({
  selector: 'app-holding',
  templateUrl: './holding.component.html',
  styleUrls: ['./holding.component.css']
})
export class HoldingComponent implements OnInit {
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  public holdingValue: any;
  public customerId: any;
  retPort: any;
  raPort: any;
  eoAcc: any;
  // --------------- Filter ------------------
  categoryList = [];
  categoryItems = [];
  categorySettings = {};
  sourceList = [];
  sourceItems = [];
  sourceSettings = {};
  result: any;
  // --------------------  Filter item --------------------------
  filter = { RetiermentPortfolio: false, InvestmentAdvisoryPortfolio: false, ExecutionOnlyAccount: false,
    equity: false, debt: false, cash: false, internalReference: false};

  constructor(private custId: CustomerDetailService) {
    this.customerId = custId.getMyCustomer();
    this.holdingValue = data.holding.filter( item => parseInt(item.Customer_ID, 10) === parseInt(this.customerId, 10) );
    /*this.holdingValue = data.holding.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    this.retPort = this.holdingValue.filter( item => item.Portfolio_Name === 'Retierment Portfolio' );
    this.raPort = this.holdingValue.filter( item => item.Portfolio_Name === 'Retierment Advisory Portfolio' );
    this.eoAcc = this.holdingValue.filter( item => item.Portfolio_Name === 'Execution Only Account' );*/
  }

  ngOnInit() {
    // ------------------ Multi-Select Dropdown -----------------------
    // Category
    this.categoryList = [
      { item_id: 'RetiermentPortfolio', item_text: 'Retierment Portfolio' },
      { item_id: 'InvestmentAdvisoryPortfolio', item_text: 'Investment Advisory Portfolio' },
      { item_id: 'ExecutionOnlyAccount', item_text: 'Execution Only Account' }
    ];
    this.categoryItems = [];
    this.categorySettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // Source
    this.sourceList = [
      { item_id: 'equity', item_text: 'Equity' },
      { item_id: 'debt', item_text: 'Debt' },
      { item_id: 'cash', item_text: 'Cash' },
      { item_id: 'internalReference', item_text: 'Internal Reference' }
    ];
    this.sourceItems = [];
    this.sourceSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    this.Custfilter();
  }

  onItemSelect(item: any) {
    this.filter[item.item_id] = true;
    this.Custfilter();
    console.log(this.filter);
  }
  onItemDeSelect(deitem: any) {
    this.filter[deitem.item_id] = false;
    this.Custfilter();
    console.log(this.filter);
  }

  // -------------------------  Filter Function --------------------------
  Custfilter() {
    this.result = this.holdingValue;
    // ----------------------- Category ---------------------------
    if (this.filter.RetiermentPortfolio || this.filter.InvestmentAdvisoryPortfolio || this.filter.ExecutionOnlyAccount  ) {
      this.result = this.result.filter(d => (d.Portfolio_Name === 'Retierment Portfolio' && this.filter.RetiermentPortfolio)
          || (d.Portfolio_Name === 'Investment Advisory Portfolio' && this.filter.InvestmentAdvisoryPortfolio)
          || (d.Portfolio_Name === 'Execution Only Account' && this.filter.ExecutionOnlyAccount)
      );
    }
    // ----------------------- Source ---------------------------
    if (this.filter.equity || this.filter.debt || this.filter.cash || this.filter.internalReference) {
      this.result = this.result.filter(d => (d.Asset_Class === 'Equity' && this.filter.equity)
          || (d.Asset_Class === 'Debt' && this.filter.debt)
          || (d.Asset_Class === 'Cash' && this.filter.cash)
          || (d.Asset_Class === 'Internal Reference' && this.filter.internalReference));
    }


/*    this.retPort = this.result.filter( item => item.Portfolio_Name === 'Retierment Portfolio' );
    this.raPort = this.result.filter( item => item.Portfolio_Name === 'Retierment Advisory Portfolio' );
    this.eoAcc = this.result.filter( item => item.Portfolio_Name === 'Execution Only Account' );*/

  }

}
