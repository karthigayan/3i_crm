import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverviewRoutingModule } from './overview-routing.module';
import { InvestmentOverviewComponent } from './investment-overview/investment-overview.component';
import {ChartModule} from '../../../_custom_components/charts/charts.module';
import { AccountDrilldownComponent } from './account-drilldown/account-drilldown.component';
import {ChartsModule} from 'ng2-charts';
import { CustomerProfileComponent } from './customer-profile/customer-profile.component';
import {NgbRatingModule, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {BarChartModule} from '@swimlane/ngx-charts';
import {SparklineModule} from '@progress/kendo-angular-charts';
import { GaugesModule } from '@progress/kendo-angular-gauges';
import { TransactionsComponent } from './transactions/transactions.component';
import { HoldingComponent } from './holding/holding.component';
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [InvestmentOverviewComponent, AccountDrilldownComponent, CustomerProfileComponent, TransactionsComponent, HoldingComponent],
    imports: [
        CommonModule,
        OverviewRoutingModule,
        ChartModule,
        ChartsModule,
        NgbTabsetModule,
        Ng2SmartTableModule,
        BarChartModule,
        NgbRatingModule,
        SparklineModule,
        GaugesModule,
        NgMultiSelectDropDownModule,
        FormsModule
    ]
})
export class OverviewModule { }
