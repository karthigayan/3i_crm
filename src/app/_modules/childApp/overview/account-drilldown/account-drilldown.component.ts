import { Component, OnInit } from '@angular/core';
import { single, multi, generateData } from './chartData';
import * as shape from 'd3-shape';
// @ts-ignore
import ApexCharts from 'apexcharts';
import { CustomerDetailService } from '../../../../_service/customer/customer-detail.service';
declare var require: any;
import topBarData from './topBar.json';
import portPerformance from './performance.json';
import exposure from './exposure.json';
import allocation from './allocationMovment.json';
import portfolioGain from './portfolioGain.json';
import chartD from './data-chart.json';


import { number } from '@amcharts/amcharts4/core';
import {currency} from 'src/app/_config/currency';

const data: any = require('./data-chart.json');

@Component({
  selector: 'app-account-drilldown',
  templateUrl: './account-drilldown.component.html',
  styleUrls: ['./account-drilldown.component.css']
})
export class AccountDrilldownComponent implements OnInit {
  // -----Spark Line -----------------
  public data1: any[] = [
    936, 968, 1025, 999, 998, 1014, 1017, 1010, 1010, 1007
  ];

  // ---------------------------  line chart portfolio performance ------
  public performValue: any;
  public areaStackChart: any;
  public areaStackValueIv: any;
  public areaStackLabel: any;
  public portfolioValue: any;
  public portfolioValuePort: any;

  // ----------- Gauge meter -------------
  public value = 50;
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  single: any[];
  multi: any[];
  dateData: any[];
  dateDataWithRange: any[];
  range = false;
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  tooltipDisabled = false;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  showGridLines = true;
  innerPadding = 0;
  autoScale = true;
  timeline = false;
  barPadding = 8;
  groupPadding = 0;
  roundDomains = false;
  maxRadius = 10;
  minRadius = 3;
  view = '';
  showLabels = true;
  explodeSlices = false;
  doughnut = false;
  arcWidth = 0.25;
  rangeFillOpacity = 0.15;

  colorScheme = {
    domain: ['#745af2', '#ffb22b', '#398bf7', '#ef5350', '#06d79c', '#cccccc']
  };
  schemeType = 'ordinal';
  filteredPortfolio: any;
  selectedCustomer: any;
  selectedCustomerManager: any;
  customerId: any;
  portfolioData: any;
  portfolios: any;
  performances: any;
  performanceData: any;
  selectedCustomerPerformance: any;
  selectedCustomerSecurityType: any;
  selectedCustomerSector: any;
  SecurityType: any;
  Sector: any;
  securityTypeChart: any;
  SecurityTypes: any;
  Sectors: any;
  porspectId: any;
  sectorChart: any;
  fundManagers: any;
  fundmanager: any;
  fundManagerChart: any;
  selectedCustomerActual: any;
  selectedCustomerRecomended: any;
  deviationActuals: any;
  deviationActual: any;
  deviationRecomendeds: any;
  deviationRecomended: any;
  deviationChart: any;
  selectedCustomerAllocation: any;
  allocationMovements: any;
  allocationMovement: any;
  equity: number[] = [];
  fixedIncome: number[] = [];
  alternate: number[] = [];
  cash: number[] = [];
  equityData: any;
  selectedCustomerportfolioGain:any;
  performanceDataPortfolioCode:any;
  allocationMovementChart:any;
  portfolioGainPortfolioCode:any;
  portfolioGainData:any;
  constructor(private custId: CustomerDetailService) {
    Object.assign(this, {
      single,
      multi
    });
    this.dateData = generateData(6, false);
    this.dateDataWithRange = generateData(2, true);
    this.customerId = this.custId.getMyCustomer();
    this.selectedCustomer = topBarData.filter(val => val.customerId == this.customerId);
    this.selectedCustomerPerformance = portPerformance.filter(val => val.customerId == this.customerId);
    this.selectedCustomerSecurityType = exposure.securityType.filter(val => val.customerId == this.customerId);
    this.selectedCustomerSector = exposure.sector.filter(val => val.customerId == this.customerId);
    this.selectedCustomerManager = exposure.fundManager.filter(val => val.customerId == this.customerId);
    this.selectedCustomerActual = exposure.actual.filter(val => val.customerId == this.customerId);
    this.selectedCustomerRecomended = exposure.recomended.filter(val => val.customerId == this.customerId);
    this.selectedCustomerAllocation = allocation.filter(val => val.customerId == this.customerId);
    this.selectedCustomerportfolioGain=portfolioGain.filter(val=>val.customerId == this.customerId);
    this.performValue = chartD.performance.filter( item => parseInt(item.Customer_ID, 10) === this.customerId );
    console.log(this.performValue);
  }

  // ----------------   Bar Chart Start -------------------------------
  // bar chart 1
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    barThickness: 10
  };

  public barChartLabels: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [{
    data: [65, 59, 80, 81, 56, 55, 40],
    label: 'Iphone 8'
  }, { data: [80, 25, 78, 60, 90, 30, 25], label: 'Iphone x' }];
  public barChartColors: Array<any> = [{ backgroundColor: 'rgba(253, 159, 179, 0.6)' }, { backgroundColor: 'rgba(159, 172, 253,0.6)' }];

  // bar chart 2
  public barChartOptions2: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    barThickness: 10
  };

  public barChartLabels2: string[] = ['January'];
  public barChartType2 = 'bar';
  public barChartLegend2 = true;

  public barChartData2: any[] = [{ data: [80], label: 'Protfolio' },
  { data: [60], label: 'MSCI World Eq.' }, { data: [40], label: 'PSE 500 Index' }];
  public barChartColors2: Array<any> = [{ backgroundColor: '#74f7a2' }, { backgroundColor: '#6f7371' }, { backgroundColor: '#609dd6' }];
  // ----------------   Bar Chart End -------------------------------

  // ----------------   line Chart Start -------------------------------
  // lineChart
  public lineChartData3: Array<any> = [{ data: [65, 39, 80, 15, 76, 35, 40], label: 'Series A' }];
  public lineChartLabels3: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions3: any = {
    responsive: true
  };
  public lineChartColors3: Array<any> = [
    {
      // dark grey
      backgroundColor: 'rgba(10,206,25,0.1)',
      borderColor: '#55ce63',
      pointBackgroundColor: '#55ce63',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#55ce63'
    }
  ];
  public lineChartLegend3 = true;
  public lineChartType3 = 'line';
  // ----------------   line Chart End -------------------------------

  // ----------------   Doughnut  Start -------------------------------
  // Doughnut1
  public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData: number[] = [350, 450, 100];
  public doughnutChartType = 'doughnut';
  public chartColors: Array<any> = [
    {
      backgroundColor: ['rgba(132,197,241,1)', '#fd9fb6', '#fde098', '#A8B3C5', '#616774'],
      hoverBackgroundColor: ['#03a9f4', '#fe6e8c', '#ff9800', '#949FB1', '#4D5360'],
      borderWidth: 0,
    }
  ];

  // Doughnut2
  public doughnutChartLabels1: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales', 'Profit'];
  public doughnutChartData1: number[] = [150, 100, 250, 50];
  public doughnutChartType1 = 'doughnut';
  public chartColors1: Array<any> = [
    {
      backgroundColor: ['rgba(132,197,241,1)', '#fd9fb6', '#fde098', '#A8B3C5', '#616774'],
      hoverBackgroundColor: ['#03a9f4', '#fe6e8c', '#ff9800', '#949FB1', '#4D5360'],
      borderWidth: 0,
    }
  ];
  // ----------------   Doughnut  End -------------------------------

  get dateDataWithOrWithoutRange() {
    if (this.range) {
      return this.dateDataWithRange;
    } else {
      return this.dateData;
    }
  }

  // line interpolation
  curve = shape.curveLinear;

  // tslint:disable-next-line:no-shadowed-variable
  select(data) {
    console.log('Item clicked', data);
  }

  onLegendLabelClick(entry) {
    console.log('Legend clicked', entry);
  }
  // events
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

  ngOnInit() {
    this.fnChangeProspect(this.selectedCustomer[0].portfolioCode);
    this.fnPortfolioPerformanceChange(this.selectedCustomerPerformance[0].period);
    // this.loadSecurityTypeChart();
    const spark1 = {
      chart: {
        type: 'area',
        // width: 300,
        height: 75,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        curve: 'straight'
      },
      fill: {
        // colors: ['#f2072b']
      },
      series: [{
        data: [90, 20, 50, 38, 56, 24, 65]
      }],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      yaxis: {
        min: 0
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      }
    };
    const options3 = {
      chart: {
        type: 'bar',
        width: 100,
        height: 80,
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '80%'
        }
      },
      series: [{
        data: [50, 50, 80, 10, 20, 25, 44, 12, 50, 90, 54]
      }],
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      xaxis: {
        crosshairs: {
          width: 1
        },
      },
      tooltip: {
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter(seriesName) {
              return '';
            }
          }
        },
        marker: {
          show: false
        }
      },
      fill: {
        colors: ['#e827e5']
      }
    };
    new ApexCharts(document.querySelector('#sparkChart'), spark1).render();
    new ApexCharts(document.querySelector('#sparkChart2'), options3).render();

    this.areaPortfolioChange('monthly');
  }

  areaPortfolioChange(event) {
    if (this.areaStackChart !== undefined) {
      this.areaStackChart.destroy();
    }
    this.areaStackValueIv = [];
    this.areaStackLabel = [];
    if (event === 'monthly') {
      this.portfolioValue = this.portfolioValuePort.filter( val => val.Period === 'Monthly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Amount)
        );
        this.areaStackLabel.push(
            obj.Data_Value
        );
      });

    } else if (event === 'quarterly') {
      this.portfolioValue = this.portfolioValuePort.filter( val => val.Period === 'Quarterly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Amount)
        );
        this.areaStackLabel.push(
            obj.Data_Value
        );
      });
    } else if (event === 'yearly') {
      this.portfolioValue = this.portfolioValuePort.filter( val => val.Period === 'Yearly' );
      this.portfolioValue.forEach(obj => {
        this.areaStackValueIv.push(
            // tslint:disable-next-line:radix
            parseInt(obj.Amount)
        );
        this.areaStackLabel.push(
            obj.Data_Value
        );
      });
    }
    this.areaPortfolio();
  }

  // --------------------  Protfolio Performance -----------------------
  areaPortfolio() {
    const options = {
      chart: {
        height: 300,
        type: 'area',
        stacked: true,
        events: {
          selection(chart, e) {
            console.log(new Date(e.xaxis.min) );
          }
        },

      },
      colors: ['#008FFB', '#00E396', '#CED4DC'],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },

      series: [{
        name: 'Amount',
        data: this.areaStackValueIv
      }
      ],
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 0.6,
          opacityTo: 0.8,
        }
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left'
      },
      xaxis: {
        categories: this.areaStackLabel
      },
    };

    this.areaStackChart = new ApexCharts(
        document.querySelector('#areaStackChart'),
        options
    );
    this.areaStackChart.render();
  }

  fnChangeProspect(value) {
    console.log(value);
    if (value != "") {
      if (this.securityTypeChart !== undefined) {
        this.securityTypeChart.destroy();
        this.securityTypeChart = [];
      }
      if (this.sectorChart !== undefined) {
        this.sectorChart.destroy();
        this.sectorChart = [];
      }
      if (this.fundManagerChart !== undefined) {
        this.fundManagerChart.destroy();
        this.fundManagerChart = [];
      }
      if (this.deviationChart !== undefined) {
        this.deviationChart.destroy();
        this.deviationChart = [];
      }
      if (this.allocationMovementChart !== undefined) {
        this.allocationMovementChart.destroy();
        this.allocationMovementChart = [];
        this.equity= [];
        this.fixedIncome = [];
        this.alternate = [];
        this.cash= [];
      }
      this.portfolios = this.selectedCustomer.filter(val => val.portfolioCode === value);
      this.portfolioData = this.portfolios[0];
      this.SecurityTypes = this.selectedCustomerSecurityType.filter(val => val.portfolioCode === value);
      this.SecurityType = this.SecurityTypes[0];
      this.Sectors = this.selectedCustomerSector.filter(val => val.portfolioCode === value);
      this.Sector = this.Sectors[0];
      this.fundManagers = this.selectedCustomerManager.filter(val => val.portfolioCode === value);
      this.fundmanager = this.fundManagers[0];
      this.deviationActuals = this.selectedCustomerActual.filter(val => val.portfolioCode === value);
      this.deviationActual = this.deviationActuals[0];
      this.deviationRecomendeds = this.selectedCustomerRecomended.filter(val => val.portfolioCode === value);
      this.deviationRecomended = this.deviationRecomendeds[0];
      this.allocationMovements = this.selectedCustomerAllocation.filter(val => val.portfolioCode === value);
      this.performanceDataPortfolioCode=this.selectedCustomerPerformance.filter(val => val.portfolioCode === value);
      this.portfolioGainPortfolioCode=this.selectedCustomerportfolioGain.filter(val => val.portfolioCode === value);
      this.portfolioValuePort=this.performValue.filter(val => val.Portfolio_ID === value);
      this.defaultSecurityTypeChart();
      this.fnDefaultSectorChart();
      this.defaultFundManagerChart();
      this.fnDefaultDeviation();
      this.defaultAllocationMovementChart();
    }
  }

  fnPortfolioPerformanceChange(value) {
    if (value != "") {
      this.performanceData = this.performanceDataPortfolioCode.filter(val => val.period === value);
      //this.performanceData=this.performances[0];
    }
  }
  fnDefaultPortfolioGainChart() {
    let categories = ["Month", "Quater", "Since Inception", "Year"];
    let dataActual = [
      this.portfolioGainPortfolioCode.Equity,
      this.portfolioGainPortfolioCode.fixedIncome,
      this.portfolioGainPortfolioCode.Alternate,
      this.portfolioGainPortfolioCode.Cash
    ].map(Number);
    let dataRecommended = [
      this.portfolioGainPortfolioCode.Equity,
      this.portfolioGainPortfolioCode.fixedIncome,
      this.portfolioGainPortfolioCode.Alternate,
      this.portfolioGainPortfolioCode.Cash
    ].map(Number);
    var options = {
      chart: {
        height: 350,
        type: 'bar',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%',
          endingShape: 'rounded'
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [{
        name: 'Actual',
        data: dataActual
      }, {
        name: 'recommended',
        data: dataRecommended
      }],
      xaxis: {
        categories: categories
      },
      // yaxis: {
      //     title: {
      //         text: '$ (thousands)'
      //     }
      // },
      fill: {
        opacity: 1

      },
      // tooltip: {
      //     y: {
      //         formatter: function (val) {
      //             return "$ " + val + " thousands"
      //         }
      //     }
      // }
    }

    this.deviationChart = new ApexCharts(
      document.querySelector("#deviationChart"),
      options
    );

    this.deviationChart.render();
  }

  fnDefaultPortfolioChart(){
     let categories = ["Equity", "Fixed Income", "Alternate", "Cash"];
    let dataActual = [
      this.deviationActual.Equity,
      this.deviationActual.fixedIncome,
      this.deviationActual.Alternate,
      this.deviationActual.Cash
    ].map(Number);
    let dataRecommended = [
      this.deviationRecomended.Equity,
      this.deviationRecomended.fixedIncome,
      this.deviationRecomended.Alternate,
      this.deviationRecomended.Cash
    ].map(Number);
    var options1 = {
      chart: {
        height: 200,
        type: 'bar',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%',
          endingShape: 'rounded'
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [{
        name: 'Actual',
        data: dataActual
      }, {
        name: 'recommended',
        data: dataRecommended
      }],
      xaxis: {
        categories: categories
      },
      // yaxis: {
      //     title: {
      //         text: '$ (thousands)'
      //     }
      // },
      fill: {
        opacity: 1

      },
      // tooltip: {
      //     y: {
      //         formatter: function (val) {
      //             return "$ " + val + " thousands"
      //         }
      //     }
      // }
    }

    this.deviationChart = new ApexCharts(
      document.querySelector("#deviationChart"),
      options1
    );

    this.deviationChart.render();
  }

  defaultAllocationMovementChart() {
    let categories = ["Current",
      "Sep-19",
      "Jul-19",
      "Apr-19",
      "Jan-19",
    ];
    let i;
    for (i = 0; i < Object.keys(this.allocationMovements).length; i++) {
      this.equity.push(this.allocationMovements[i].equity);
      this.fixedIncome.push(this.allocationMovements[i].fixedIncome);
      this.alternate.push(this.allocationMovements[i].alternate);
      this.cash.push(this.allocationMovements[i].cash);
    }
    var options = {
      chart: {
        height: 300,
        type: 'area',
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      series: [{
        name: 'Equity',
        data: this.equity.map(Number)
      }, {
        name: 'Fixed Income',
        data: this.fixedIncome.map(Number)
      }, {
        name: 'Alternate',
        data: this.alternate.map(Number)
      }, {
        name: 'Cash',
        data: this.cash.map(Number)
      }],

      xaxis: {
        type: 'Period',
        categories: categories,
      }
    }

    this.allocationMovementChart = new ApexCharts(
      document.querySelector("#allocationMovementChart"),
      options
    );

    this.allocationMovementChart.render();
  }
  defaultFundManagerChart() {
    let fundManagerlabels = ['Vanguard', 'T Rowe Price', 'Blackrock', 'SunLife', 'CIMB', 'RAMPVER', 'ATRAM', 'KENANGA', 'EASTSPRING', 'OTHERS'];
    let fundManagerValues = [
      this.fundmanager.PNB,
      this.fundmanager.PAMI,
      this.fundmanager.FMIC,
      this.fundmanager.SunLife,
      this.fundmanager.ALFM,
      this.fundmanager.RAMPVER,
      this.fundmanager.ATRAM,
      this.fundmanager.COCOLIFE,
      this.fundmanager.GREPALIFE,
      this.fundmanager.OTHERS
    ].map(Number);
    var options1 = {
      labels: fundManagerlabels,
      chart: {
        type: 'donut',
        // width: 400,
        height: 350,
      },
      series: fundManagerValues,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {

          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    }

    this.fundManagerChart = new ApexCharts(
      document.querySelector("#fundManagerChart"),
      options1
    );
    this.fundManagerChart.render();
  }
  defaultSecurityTypeChart() {
    let Securitylabels = ["Direct Equity", "Equity Mutual Fund", "Debt Mutual Fund", "Alternate Investment Fund", "Money Market Fund", "Real Estate", "Bonds", "Cash"];
    let SecurityValues = [this.SecurityType.directEquity,
    this.SecurityType.equityMutualFund,
    this.SecurityType.debtMutualFund,
    this.SecurityType.alternateInvestmentFund,
    this.SecurityType.MoneyMarketFund,
    this.SecurityType.realEstate,
    this.SecurityType.Bonds,
    this.SecurityType.Cash
    ].map(Number);
    console.log(SecurityValues);
    var options1 = {
      labels: Securitylabels,
      chart: {
        type: 'donut',
        // width: 400,
        height:350,
      },
      series: SecurityValues,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {

          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    }

    this.securityTypeChart = new ApexCharts(
      document.querySelector("#securityTypeChart"),
      options1
    );
    this.securityTypeChart.render();
  }
  fnDefaultSectorChart() {
    let categories = ['Banking and finance', 'Technology', 'Brewery & Tobacco', 'Telecommunication', 'Hospitality', 'Auto', 'Power', 'Energy', 'Media', 'Pharmaceutical', 'Real Estate', 'Others']
    let datas = [this.Sector.bankingAndFinance,
    this.Sector.Diversified,
    this.Sector.breweryTobacco,
    this.Sector.Telecommunication,
    this.Sector.Hospitality,
    this.Sector.Food,
    this.Sector.Power,
    this.Sector.Energy,
    this.Sector.Mining,
    this.Sector.Shipping,
    this.Sector.realEstate,
    this.Sector.Others
    ].map(Number);
    var options = {
      chart: {
        height: 380,
        type: 'bar'
      },
      plotOptions: {
        bar: {
          barHeight: '100%',
          distributed: true,
          horizontal: true,
          dataLabels: {
            position: 'bottom'
          },
        }
      },
      colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e', '#f48024', '#69d2e7'],
      dataLabels: {
        enabled: true,
        textAnchor: 'start',
        style: {
          colors: ['#fff']
        },
        formatter: function (val, opt) {
          return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
        },
        offsetX: 0,
        dropShadow: {
          enabled: true
        }
      },
      series: [{
        data: datas
      }],
      stroke: {
        width: 1,
        colors: ['#fff']
      },
      xaxis: {
        categories: categories,
      },
      yaxis: {
        labels: {
          show: false
        }
      },
      // title: {
      //   text: 'Custom DataLabels',
      //   align: 'center',
      //   floating: true
      // },

      tooltip: {
        theme: 'dark',
        x: {
          show: false
        },
        y: {
          title: {
            formatter: function () {
              return ''
            }
          }
        }
      }
    }

    this.sectorChart = new ApexCharts(
      document.querySelector("#sectorChart"),
      options
    );
    this.sectorChart.render();
  }
  fnDefaultDeviation() {
    let categories = ["Equity", "Fixed Income", "Alternate", "Cash"];
    let dataActual = [
      this.deviationActual.Equity,
      this.deviationActual.fixedIncome,
      this.deviationActual.Alternate,
      this.deviationActual.Cash
    ].map(Number);
    let dataRecommended = [
      this.deviationRecomended.Equity,
      this.deviationRecomended.fixedIncome,
      this.deviationRecomended.Alternate,
      this.deviationRecomended.Cash
    ].map(Number);
    var options = {
      chart: {
        height: 300,
        type: 'bar',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%',
          endingShape: 'rounded'
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [{
        name: 'Actual',
        data: dataActual
      }, {
        name: 'recommended',
        data: dataRecommended
      }],
      xaxis: {
        categories: categories
      },
      // yaxis: {
      //     title: {
      //         text: '$ (thousands)'
      //     }
      // },
      fill: {
        opacity: 1

      },
      // tooltip: {
      //     y: {
      //         formatter: function (val) {
      //             return "$ " + val + " thousands"
      //         }
      //     }
      // }
    }

    this.deviationChart = new ApexCharts(
      document.querySelector("#deviationChart"),
      options
    );

    this.deviationChart.render();
  }

}
