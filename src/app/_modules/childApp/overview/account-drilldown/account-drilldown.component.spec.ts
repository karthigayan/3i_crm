import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDrilldownComponent } from './account-drilldown.component';

describe('AccountDrilldownComponent', () => {
  let component: AccountDrilldownComponent;
  let fixture: ComponentFixture<AccountDrilldownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDrilldownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDrilldownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
