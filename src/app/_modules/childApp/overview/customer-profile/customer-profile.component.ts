import { Component, OnInit } from '@angular/core';
import * as tableData from './customerData';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.component.html',
  styleUrls: ['./customer-profile.component.css']
})
export class CustomerProfileComponent implements OnInit {

  source: LocalDataSource;
  constructor() {
    this.source = new LocalDataSource(tableData.data); // create the source
  }
  settings = tableData.settings;

  ngOnInit() {
  }

}
