import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InvestmentOverviewComponent} from './investment-overview/investment-overview.component';
import {AccountDrilldownComponent} from './account-drilldown/account-drilldown.component';
import {CustomerProfileComponent} from './customer-profile/customer-profile.component';
import {TransactionsComponent} from './transactions/transactions.component';
import {HoldingComponent} from './holding/holding.component';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: 'customer',
        component: InvestmentOverviewComponent
      },
      {
        path: 'drilldown',
        component: AccountDrilldownComponent
      },
      {
        path: 'profile',
        component: CustomerProfileComponent
      },
      {
        path: 'transactions',
        component: TransactionsComponent
      },
      {
        path: 'holding',
        component: HoldingComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OverviewRoutingModule { }
