import { Component, OnInit } from '@angular/core';
import data from './data.json';
import chart from './chart.json';
import {Router} from '@angular/router';
// @ts-ignore
import ApexCharts from 'apexcharts';

@Component({
  selector: 'app-my-prospect',
  templateUrl: './my-prospect.component.html',
  styleUrls: ['./my-prospect.component.css']
})
export class MyProspectComponent implements OnInit {
  customer: any = data;
  chartData: any = chart;
  public result: any;

  // --------------- Filter ------------------
  categoryList = [];
  categoryItems = [];
  categorySettings = {};
    sourceList = [];
    sourceItems = [];
    sourceSettings = {};
  // --------------------  Filter item --------------------------
  filter = { overseasFilipinos: false, residentIndividual: false, familyTrust: false,
        campaign: false, ownSourcing: false, walkin: false, internalReference: false,
        cold: false, hot: false, notQualified: false, qualified: false };
  constructor(private router: Router) {}

  ngOnInit() {
    this.prospectTop1();
    this.prospectTop2();
    // ------------------ Multi-Select Dropdown -----------------------
    // Category
    this.categoryList = [
      { item_id: 'overseasFilipinos', item_text: 'Overseas Filipinos' },
      { item_id: 'residentIndividual', item_text: 'Resident Individual' },
      { item_id: 'familyTrust', item_text: 'Family Trust' }
    ];
    this.categoryItems = [];
    this.categorySettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // Source
    this.sourceList = [
          { item_id: 'campaign', item_text: 'Campaign' },
          { item_id: 'ownSourcing', item_text: 'Own Sourcing' },
          { item_id: 'walkin', item_text: 'Walk IN' },
          { item_id: 'internalReference', item_text: 'Internal Reference' }
      ];
    this.sourceItems = [];
    this.sourceSettings = {
          singleSelection: false,
          idField: 'item_id',
          textField: 'item_text',
          selectAllText: 'Select All',
          itemsShowLimit: 1,
          allowSearchFilter: true
      };
    this.result = this.customer;
    this.Custfilter();
  }
  onItemSelect(item: any) {
      this.filter[item.item_id] = true;
      this.Custfilter();
      console.log(this.filter);
  }
  onItemDeSelect(deitem: any) {
      this.filter[deitem.item_id] = false;
      this.Custfilter();
      console.log(this.filter);
  }
  onSelectAll(items: any) {
  }
    // -------------------- Prospect Chart ---------------------------
    prospectTop1() {
        const options = {
            chart: {
                height: 200,
                width: 200,
                type: 'radialBar',
            },
            plotOptions: {
                radialBar: {

                    dataLabels: {
                        name: {
                            fontSize: '10px',
                        },
                        value: {
                            show: true,
                            fontSize: '8',
                            fontFamily: undefined,
                            color: undefined,
                            offsetY: 16,
                            formatter(val) {
                                return val;
                            }
                        },
                        show: true,
                        fontSize: '8',
                        fontFamily: undefined,
                        color: undefined,
                        offsetY: 16,
                        formatter(val) {
                            return val;
                        },
                        total: {
                            show: true,
                            label: 'Total',
                            formatter(w) {
                                return chart.top1.total;
                            }
                        }
                    }
                }
            },
            series: chart.top1.values,
            labels: chart.top1.lable,

        };
        const top1 = new ApexCharts(
            document.querySelector('#top1'),
            options
        );

        top1.render();
    }
    // -------------------- Prospect Chart ------------------------
    prospectTop2() {
        const options = {
            chart: {
                height: 200,
                width: 200,
                type: 'radialBar',
            },
            plotOptions: {
                radialBar: {

                    dataLabels: {
                        name: {
                            fontSize: '10px',
                        },
                        value: {
                            show: true,
                            fontSize: '8',
                            fontFamily: undefined,
                            color: undefined,
                            offsetY: 16,
                            formatter(val) {
                                return val;
                            }
                        },
                        total: {
                            show: true,
                            label: 'Total',
                            formatter(w) {
                                return chart.top2.total;
                            }
                        }
                    }
                }
            },
            series: chart.top2.values,
            labels: chart.top2.lable,
        };
        const top2 = new ApexCharts(
            document.querySelector('#top2'),
            options
        );
        top2.render();
    }
  onNavigate() {
    this.router.navigate(['/overview']);
  }

// -------------------------  Filter Function --------------------------
  Custfilter() {
    this.result = data;
    // ----------------------- Category ---------------------------
    if (this.filter.overseasFilipinos || this.filter.residentIndividual || this.filter.familyTrust  ) {
      this.result = data.filter(d => (d.Category === 'Overseas Filipinos' && this.filter.overseasFilipinos)
          || (d.Category === 'Resident Individual' && this.filter.residentIndividual)
          || (d.Category === 'Family Trust' && this.filter.familyTrust)
      );
    }
    // ----------------------- Source ---------------------------
    if (this.filter.campaign || this.filter.ownSourcing || this.filter.walkin || this.filter.internalReference) {
      this.result = this.result.filter(d => (d.Source === 'Campaign' && this.filter.campaign)
          || (d.Source === 'Own Sourcing' && this.filter.ownSourcing)
          || (d.Source === 'Walk IN' && this.filter.walkin)
          || (d.Source === 'Internal Reference' && this.filter.internalReference));
    }
    // ------------------------  Hot / Cold ---------------------
    if (this.filter.hot || this.filter.cold ) {
      this.result = this.result.filter(d => (d.Hot_Cold === 'Hot' && this.filter.hot)
          || (d.Hot_Cold === 'Cold' && this.filter.cold));
    }
    // ------------------------  Qualified ---------------------
    if (this.filter.notQualified || this.filter.qualified ) {
      this.result = this.result.filter(d => (d.Qulalification === 'Not Qualified' && this.filter.notQualified)
          || (d.Qulalification === 'Qualified' && this.filter.qualified));
    }
  }
}
