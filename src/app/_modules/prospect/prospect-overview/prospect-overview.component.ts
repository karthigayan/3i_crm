import { Component, OnInit } from '@angular/core';
import {currency} from '../../../_config/currency';

import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist/dist/chartist.component';
// @ts-ignore
import ApexCharts from 'apexcharts';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
declare var require: any;
const data: any = require('./data.json');
@Component({
  selector: 'app-prospect-overview',
  templateUrl: './prospect-overview.component.html',
  styleUrls: ['./prospect-overview.component.css']
})
export class ProspectOverviewComponent implements OnInit {
  userDatas: any;
  demographicValues: any;
  demographicValues1: any;
  demographics: any;
  demographics1: any;
  userData: any;
  currency = currency.BASE_CUR;
  cSymbol = currency.BASE_CUR_SYL;
  public activity = data.prospect_activity;
  notQualifiedStyles = {
    width: data.progressBar.percentage[0] + '%',
    height: '6px'
  };
  qualifiedStyles = {
    width: data.progressBar.percentage[1] + '%',
    height: '6px'
  };
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.prospectTop1();
    this.prospectTop2();
    this.loadcreationTrend();
    this.prospectSource1();
    this.prospectSource2();
    this.demographicsPieChart();
    this.userData = data.userData.filter((item, index) => index => 0 );
    this.userDatas = data.userData;
    this.fnDemographicsChange('Category');
  }
  // -------------------- Prospect Chart ---------------------------
  prospectTop1() {
    const options = {
      chart: {
        height: 200,
        width: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {

          dataLabels: {
            name: {
              fontSize: '10px',
            },
            value: {
              show: true,
              fontSize: '8',
              fontFamily: undefined,
              color: undefined,
              offsetY: 16,
              formatter(val) {
                return val;
              }
            },
            show: true,
            fontSize: '8',
            fontFamily: undefined,
            color: undefined,
            offsetY: 16,
            formatter(val) {
              return val;
            },
            total: {
              show: true,
              label: 'Total',
              formatter(w) {
                 return data.top1.total;
              }
            }
          }
        }
      },
      series: data.top1.values,
      labels: data.top1.lable,

    };
    const top1 = new ApexCharts(
        document.querySelector('#top1'),
        options
    );

    top1.render();
  }
  // -------------------- Prospect Chart ------------------------
  prospectTop2() {
    const options = {
      chart: {
        height: 200,
        width: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {

          dataLabels: {
            name: {
              fontSize: '10px',
            },
            value: {
              show: true,
              fontSize: '8',
              fontFamily: undefined,
              color: undefined,
              offsetY: 16,
              formatter(val) {
                return val;
              }
            },
            total: {
              show: true,
              label: 'Total',
              formatter(w) {
                 return data.top2.total;
              }
            }
          }
        }
      },
      series: data.top2.values,
      labels: data.top2.lable,
    };
    const top2 = new ApexCharts(
        document.querySelector('#top2'),
        options
    );
    top2.render();
  }
  loadcreationTrend() {
    const creationTrendOption = {
      chart: {
        height: 300,
        type: 'line',
        zoom: {
          enabled: false
        }
      },
      series: [{
        name: 'Conversion Trend',
        data: data.creationTrend.values
      }],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      title: {
        text: 'Conversion Trend by Month',
        align: 'left'
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {
        categories: data.creationTrend.lable,
      }
    };
    const creationTrend = new ApexCharts(
        document.querySelector('#creationTrend'),
        creationTrendOption
    );
    creationTrend.render();
  }
  prospectSource1() {
    const options = {
      chart: {
        width: 450,
        height: 450,
        type: 'pie',
      },
      labels: data.prospectSource1.lables,
      series: data.prospectSource1.values,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    const proSource1 = new ApexCharts(
        document.querySelector('#proSource1'),
        options
    );

    proSource1.render();
  }
  prospectSource2() {
    const options1 = {
      labels: data.prospectSource2.labels,
      chart: {
        type: 'donut',
        width: 438,
        height: 438
      },
      series: data.prospectSource2.values,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {

          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    const proSource2 = new ApexCharts(
        document.querySelector('#proSource2'),
        options1
    );
    proSource2.render();
  }
  // -------------- Demographic Pie Chart ---------------
  demographicsPieChart() {
    const options = {
      chart: {
        width: 350,
        type: 'pie',
      },
      labels: data.demographicsCategory.labels,
      series: data.demographicsCategory.values,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    this.demographics = new ApexCharts(
        document.querySelector('#demographics'),
        options
    );

    this.demographics.render();
  }
  fnDemographicsChange(value) {
    if (this.demographics1 !== undefined) {
      this.demographics1.destroy();
    }
    this.demographicValues = [];
    if (value === 'Category') {
      this.demographicValues = data.demographicsCategory;
    } else if (value === 'Location') {
      this.demographicValues = data.demographicsLocation;
    } else if (value === 'Occupation') {
      this.demographicValues = data.demographicsOccupation;
    }
    this.demographicDefaultChar();
  }
  demographicDefaultChar() {
    const deoptions = {
      chart: {
        width: 400,
        height: 400,
        type: 'pie',
      },
      labels:  this.demographicValues.labels,
      series:  this.demographicValues.values,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    };

    this.demographics1 = new ApexCharts(
        document.querySelector('#demographics1'),
        deoptions
    );

    this.demographics1.render();
  }
  // --------------------------  Modal For High Value -----------------
  openUser(content1) {
    this.modalService.open(content1, {
      size: 'lg',
      centered: true,
    });

  }
}
