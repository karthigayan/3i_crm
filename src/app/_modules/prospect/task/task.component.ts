import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  frequency: boolean;
  timeHide: boolean;

  constructor() { }

  ngOnInit() {
    this.timeHide=true;
    this.frequency = false;
  }
  checkValue(value) {
    if (value == "on") {
      this.timeHide = false;
    }
    else {
      this.timeHide = true;
    }
  }
  RepeatChange(value){
    if(value=="on"){
      this.frequency = true;
    }
    else{
      this.frequency = false;
    }
  }
}
