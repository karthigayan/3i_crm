import { Component, OnInit } from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";
declare var require: any;
const data: any = require('./data.json');
@Component({
  selector: 'app-add-prospect',
  templateUrl: './add-prospect.component.html',
  styleUrls: ['./add-prospect.component.css']
})
export class AddProspectComponent implements OnInit {
  campaignName: any;
  campaignType: any;
  Occupation: any;
  Category: any;
  Salutation: any;
  Source: any;
  Country: any;
  Gender: any;
  userType: any;
  campaignTypeValue: any;
  campaignValue: any;
  typeValue: any;

  constructor(private modalService: NgbModal,
              private router: Router) { }

  ngOnInit() {
    this.Salutation = data.Salutation.value;
    this.userType = data.userType.value;
    this.Gender = data.Gender.value;
    this.Country = data.Country.value;
    this.Source = data.Source.value;
    console.log(data);
  }
  saveContact(content3) {
    this.modalService.open(content3, { centered: true }).result.then((result) => {
      console.log(result);
      if (result === 'Close') {
        this.router.navigate(['/prospect/myprospect']);
      }

    }, (reason) => {
      // this.router.navigate(['/prospect/myprospect']);
    });
  }

  cancelContact() {
    this.router.navigate(['/prospect/myprospect']);

  }

/*  clickEvent(link, content3) {
    // @ts-ignore
    this.modalService.dismissAll(content3, { "centered": true });
    this.router.navigate([link]);

  }*/

  fnChangeType(type) {
   
    if (type == "Individual") {
      this.Category = data.individualCategory.value;
      this.Occupation = data.individualOccupation.value;
      console.log(data.individualCategory.value);
    }
    else {
      this.Category = data.corporateCategory.value;
      this.Occupation = data.corporateSector.value;
    }
  }
  fnChangeCampaign(campaign) {
    this.campaignValue = campaign;
    this.campaignType= data.CampaignType.value;
  }
  fnChangeCampaignType(campaignType) {
    if (campaignType == "Event & Exhibition") {
      this.campaignName = data.campaingNameEvent.value;
    }
    else if(campaignType == "Email Campaign"){
      this.campaignName = data.campaingNameEmail.value;
    }
    else if(campaignType == "Digital Marketing"){
      this.campaignName = data.campaingNameDigital.value;
    }
    else if(campaignType == "Advertising"){
      this.campaignName = data.campaingNameAdvertising.value;
    }
  }
}
