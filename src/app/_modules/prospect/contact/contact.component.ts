import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, NgForm, FormControl} from '@angular/forms';
import { Validatable } from '@amcharts/amcharts4/.internal/core/utils/Validatable';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
declare var require: any;
const data: any = require('./data.json');
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  submitted: boolean;
  cityValue: any;
  stateValue: any;
  county: any;
  countryCodes;
  contactForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private router: Router) { }

  ngOnInit() {
    this.countryCodes = data.countryCodes.value;
    this.county = data.country.value;
    this.contactForm = this.formBuilder.group({
      type: [''],
      employee: [''],
      salutation: [''],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      countryCodeMobile: [''],
      mobile: ['', Validators.pattern(/^[6-9]\d{9}$/)],
      workEmail: ['', Validators.email],
      countryCodeDesk: [''],
      desk: ['', Validators.pattern(/^[6-9]\d{9}$/)],
      personalEmail: ['', Validators.email],
      countryCodeFax: [''],
      fax: ['', Validators.pattern(/^[6-9]\d{9}$/)],
      otherEmail: ['', Validators.email]
    });
  }
  saveContact(content3) {
    console.log(content3);
    this.modalService.open(content3, { centered: true }).result.then((result) => {
      this.router.navigate(['/profileDetails']);
    }, (reason) => {
      this.router.navigate(['/profileDetails']);
    });
  }
  cancelContact() {
      this.router.navigate(['/profileDetails']);

  }
  get f() {
    return this.contactForm.controls;
  }
  onSubmit() {
    this.submitted = true;
  }
  fnCountryChange(value) {
    if (value.value === 'India') {
      this.stateValue = data.state.India;
    } else if (value.value === 'Philippines') {
      this.stateValue = data.state.Philippines;
    } else if (value.value === 'USA') {
      this.stateValue = data.state.USA;
    } else {
      this.stateValue = '';
    }
  }
  fnStateChange(value) {
    if (value.value === 'Mumbai') {
      this.cityValue = data.cities.Mumbai;
    } else if (value.value === 'Kolkata') {
      this.cityValue = data.cities.Kolkata;
    } else if (value.value === 'California') {
      this.cityValue = data.cities.California;
    } else if (value.value === 'New York') {
      this.cityValue = data.cities.NewYork;
    } else if (value.value === 'Abra') {
      this.cityValue = data.cities.Abra;
    } else if (value.value === 'Albay') {
      this.cityValue = data.cities.Albay;
    } else {
      this.cityValue = '';
    }
  }
}
