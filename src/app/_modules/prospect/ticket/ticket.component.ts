import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {
  subjectDD: boolean;

  constructor() { }

  ngOnInit() {
    this.subjectDD = false;
  }
  fnAreaChange(value) {
    if (value == "Account Service") {
      this.subjectDD = true;
    }
    else {
      this.subjectDD = false;
    }
  }
}
