import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProspectOverviewComponent} from './prospect-overview/prospect-overview.component';
import {MyProspectComponent} from './my-prospect/my-prospect.component';
import {AddProspectComponent} from './add-prospect/add-prospect.component';
import {ContactComponent} from './contact/contact.component';
import {PotentialComponent} from './potential/potential.component';
import {ActivityComponent} from './activity/activity.component';
import {TaskComponent} from './task/task.component';
import {TicketComponent} from './ticket/ticket.component';

const routes: Routes = [
  {path: '', children: [
          {path: '', component: ProspectOverviewComponent},
          {path: 'myprospect', component: MyProspectComponent},
          {path: 'addprospect', component: AddProspectComponent},
          {path: 'contact', component: ContactComponent},
          {path: 'potential', component: PotentialComponent},
          {path: 'activity', component: ActivityComponent},
          {path: 'task', component: TaskComponent},
          {path: 'ticket', component: TicketComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProspectRoutingModule { }
