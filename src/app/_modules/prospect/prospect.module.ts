import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProspectRoutingModule } from './prospect-routing.module';
import { ProspectOverviewComponent } from './prospect-overview/prospect-overview.component';
import { MyProspectComponent } from './my-prospect/my-prospect.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng5SliderModule} from 'ng5-slider';
import { AddProspectComponent } from './add-prospect/add-prospect.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {MaterialModuleModule} from '../../_custom_components/material-module/material-module.module';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {ContactComponent} from './contact/contact.component';
import {PotentialComponent} from './potential/potential.component';
import {TaskComponent} from './task/task.component';
import {ActivityComponent} from './activity/activity.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TicketComponent} from './ticket/ticket.component';

@NgModule({
  declarations: [ProspectOverviewComponent, MyProspectComponent, AddProspectComponent,
      ContactComponent, PotentialComponent, TaskComponent, ActivityComponent, TicketComponent],
    imports: [
        CommonModule,
        ProspectRoutingModule,
        FormsModule,
        Ng5SliderModule,
        NgMultiSelectDropDownModule,
        MaterialModuleModule,
        NgxMaterialTimepickerModule,
        NgbModule,
        ReactiveFormsModule
    ]
})
export class ProspectModule { }
