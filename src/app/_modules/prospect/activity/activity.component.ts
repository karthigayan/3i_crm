import { Component, OnInit } from '@angular/core';
import {NgbDateStruct, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  frequency: boolean;
  timeHide: boolean;
  model: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;


  constructor(private modalService: NgbModal,
              private router: Router) { }

  ngOnInit() {
    this.timeHide = true;
    this.frequency = false;
  }

  saveContact(content3) {
    this.modalService.open(content3, { centered: true }).result.then((result) => {
      if (result === 'Close') {
        this.router.navigate(['/engagement/myactivities']);
      }
    }, (reason) => {
      // this.router.navigate(['/prospect/myprospect']);
    });
  }

  cancelContact() {
    this.router.navigate(['/engagement/myactivities']);

  }

  checkValue(value) {
    this.timeHide = value !== 'on';
  }
  RepeatChange(value) {
    this.frequency = value === 'on';
  }
}
