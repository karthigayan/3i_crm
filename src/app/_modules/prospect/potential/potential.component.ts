import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";
declare var require: any;
const data: any = require('./data.json');
@Component({
  selector: 'app-potential',
  templateUrl: './potential.component.html',
  styleUrls: ['./potential.component.css']
})
export class PotentialComponent implements OnInit {
  noOfDays;
  probability;
  date: string;
  investementAmountSet;
  resultDD;
  otherFiduciaryDD;
  otherInvestmentDD;
  stages: any;
  currencyCode: any;
  otherFiduciary: any;
  otherInvestment: any;
  oppurtunityType: any;

  constructor(private modalService: NgbModal,
              private router: Router) { }

  ngOnInit() {
    this.oppurtunityType = data.oppurtunityType.value;
    this.stages = data.stages.value;
    this.otherInvestment = data.otherInvestment.value;
    this.otherFiduciary = data.otherFiduciaryService.value;
    this.currencyCode = data.currencyCode.value;
  }

  saveContact(content3) {
    this.modalService.open(content3, { centered: true }).result.then((result) => {
      console.log(result);
      if (result === 'Close') {
        this.router.navigate(['/potentials/mypotentials']);
      }

    }, (reason) => {
      // this.router.navigate(['/prospect/myprospect']);
    });
  }

  cancelContact() {
    this.router.navigate(['/potentials/mypotentials']);

  }

  fnOppurtunityTypeChange(value) {
    console.log(value);
    if (value == "Other Investment Products") {
      this.otherInvestmentDD = true;
      this.otherFiduciaryDD = false;
    }
    else if (value == "Other Fiduciary Services") {
      this.otherInvestmentDD = false;
      this.otherFiduciaryDD = true;
    }
    else{
      this.otherInvestmentDD = false;
      this.otherFiduciaryDD = false;
    }
  }
  fnStatusChange(value) {
    if (value == "Closed") {
      this.resultDD = true;
    }
    else{
      this.resultDD = false;
    }
  }
  fnResultChange(value) {
    if (value == "Won") {
      this.investementAmountSet = true;
    }
    else{
      this.investementAmountSet = false;
    }
  }
  fnStageChange(value) {
    switch (value) {
case "Introduction 10%":
this.date=formatDate(new Date(),'dd-MM-yyyy','en');
this.probability= "10%";
this.noOfDays="0";
break;
case "Qualification 20%":
this.date=formatDate(new Date(),'dd-MM-yyyy','en');
this.probability= "20%";
this.noOfDays="0";
break;
case "Need Analysis 40%":
this.date=formatDate(new Date(),'dd-MM-yyyy','en');
this.probability= "40%";
this.noOfDays="0";
break;
case "Advanced Discussion 60%":
this.date=formatDate(new Date(),'dd-MM-yyyy','en');
this.probability= "60%";
this.noOfDays="0";
break;
case "Final Evaluation 80%":
this.date=formatDate(new Date(),'dd-MM-yyyy','en');
this.probability= "80%";
this.noOfDays="0";
break;
    }
  }

}
