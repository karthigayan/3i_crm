import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LayoutsModule} from './_custom_components/layouts/layouts.module';
import {RouterModule} from '@angular/router';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModuleModule} from './_custom_components/material-module/material-module.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { BlankComponent } from './_custom_components/pages/blank/blank.component';
import { FullComponent } from './_custom_components/pages/full/full.component';
import { Approutes } from './app-routing.module';
import { ChildComponent } from './_custom_components/pages/child/child.component';
import { ChartsModule } from '@progress/kendo-angular-charts';
import 'hammerjs';
import {CustomerDetailService} from './_service/customer/customer-detail.service';



const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
    wheelSpeed: 2,
    wheelPropagation: true
};

@NgModule({
  declarations: [
    AppComponent,
    BlankComponent,
    FullComponent,
    ChildComponent
  ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        LayoutsModule,
        RouterModule,
        PerfectScrollbarModule,
        RouterModule.forRoot(Approutes),
        MaterialModuleModule,
        FlexLayoutModule,
        ChartsModule

    ],
  providers: [
      {provide: LocationStrategy, useClass: HashLocationStrategy},
      {
          provide: PERFECT_SCROLLBAR_CONFIG,
          useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
      },
      CustomerDetailService
      ],
  bootstrap: [AppComponent]
})
export class AppModule {}
